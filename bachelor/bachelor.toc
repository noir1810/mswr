\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Vorwort}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Von \textit {Art Gallery Problem} und \textit {watchmen Route}...}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}... zum \textit {Surveillance Route}.}{4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Einleitung}{5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Grundlagen}{5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Grundlagen der \textit {watchman route}}{5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Grundlagen zum \textit {surveillance Route}}{7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Angewandte Algorithmen}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Triangulation}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline \textit {Trapezoidal Map}}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline \textit {Double Connected Edge List}}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline \textit {Triangulierung von x-monotone Polygone}}{14}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}\textit {Ray shooting}}{17}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Hauptteil}{19}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Konstruktion einer \textit {watchman} Route}{19}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline \textit {essential cuts} bestimmen}{19}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}\textit {Optimum surveillance Route} bestimmen}{22}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Schluss}{25}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Implementierung}{25}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Historie und verwandte Arbeiten}{25}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Fazit und Ausblick}{25}
