package Triangulation;

import DataStructures.DoubleResult;
import DataStructures.Polygon;
import DoubleConnectedEdge.DCELStructure;
import DoubleConnectedEdge.Face;
import DoubleConnectedEdge.HalfEdge;
import Exceptions.HalfEdgeNotFound;
import Exceptions.SSNodeFalseClassException;
import Exceptions.TrapezoidNotFoundException;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class Triangulation{
    Polygon poly;
    Trapezoidalization trapzoids;
    DCELStructure hEdge;
    List<Line2D.Double> drawTriangle = new ArrayList<>();


    public Triangulation(Polygon poly){
        this.poly = poly;
        hEdge = new DCELStructure(this.poly);
        trapzoids = new Trapezoidalization(poly.edgeList);
    }

    public Triangulation(List<Line2D.Double> poly){
        trapzoids = new Trapezoidalization(poly);
    }

    // log2:  Logarithm base 2
    public static double log2(double d) {
        return Math.log(d)/Math.log(2.0);
    }

    // Get log*n for given n
    public static int math_logstar_n(double n){
        int i;
        double v;

        for (i = 0, v = (double) n; v >= 1; i++)
            v = log2(v);

        return (i-1);
    }

    public void find_new_roots(int segNum){

        segNum--;
        Trapezoidalization.Seg line = trapzoids.lineSegments.get(segNum);
        SSNode root;
        if (line.isInserted == true)
            return;

        try {
            if(line.rootA==null)
                root = trapzoids.root;
            else root = line.rootA;
            line.rootA = trapzoids.searchTrapezoid(line.a, line.b,root);
        }catch (SSNodeFalseClassException | TrapezoidNotFoundException e){
            System.err.println(e);
        }
        try {
            if(line.rootB==null)
                root = trapzoids.root;
            else root = line.rootB;
            line.rootB = trapzoids.searchTrapezoid(line.b, line.a,root);
        }catch (SSNodeFalseClassException | TrapezoidNotFoundException e){
            System.err.println(e);
        }

    }

    public static double math_N(double n, double h){
        double v;
        int i;
        for (i = 0, v = (int) n; i < h; i++)
            v = log2(v);
        return (int) Math.ceil((double) 1.0*n/v);
    }

    /* Main routine to perform trapezoidation */
    public void build_trapezoidization() {

        int i;
        int h;
        int maxSeg = this.poly.edgeList.size();
        Trapezoidalization.Seg next;

          /* Add the first segment and get the query structure and trapezoid */
          /* list initialised */

        for (h = 1; h <= math_logstar_n(maxSeg); h++) {
            System.out.println("oter logstar "+math_logstar_n(maxSeg));
            for (i = (int) math_N(maxSeg, h - 1) + 1; i <= math_N(maxSeg, h); i++) {
                System.out.println("inner MathN "+math_N(maxSeg, h)+" "+math_N(maxSeg, h-1));
                next = this.trapzoids.get_nextSeg();
                this.trapzoids.addLineSegment(next);
            }
            /* Find a new root for each of the segment endpoints */
            for (i = 1; i <= maxSeg; i++)
                find_new_roots(i);
        }

        for (i = (int)math_N(maxSeg, math_logstar_n(maxSeg)) + 1; i <= maxSeg; i++) {
            next = this.trapzoids.get_nextSeg();
            this.trapzoids.addLineSegment(next);
        }

    }

    public boolean isPoint_onLine(Line2D line, Point2D p) {
        if(line.ptLineDist(p)<= 0.01){
            return true;
        }else return false;

    }

    private Line2D.Double isRightAndLeftPoint_on_sameLine(Trapezoid trap){
        if(trap.getBottom()==null || trap.top == null)
            return null;


        if(trap.bottom.getP1().equals(trap.top.getP1()))
            if(trap.rightP.equals(trap.bottom.getP2())||trap.rightP.equals(trap.top.getP2()))
                return null;
        if(trap.bottom.getP2().equals(trap.top.getP2()))
            if(trap.leftP.equals(trap.bottom.getP1())||trap.leftP.equals(trap.top.getP1()))
                return null;
        if(isPoint_onLine(trap.getBottom(), trap.getLeftP()) && isPoint_onLine(trap.getBottom(),trap.getRightP())){
            return null;
        }
        if(isPoint_onLine(trap.getTop(), trap.getLeftP()) && isPoint_onLine(trap.getTop(),trap.getRightP())){
            return null;
        }
        return new Line2D.Double(trap.getLeftP(), trap.getRightP());
    }


    private boolean isDrawable(Trapezoid trap, Line2D line){
        boolean bool = false;
        DoubleResult<HalfEdge> res = null;
        try {
            res = this.hEdge.search_in_DCELS(line, hEdge.getStart());
        }catch (HalfEdgeNotFound e){

        }

        if(res!=null){
            HalfEdge h1 = res.getRes1();
            HalfEdge h2 = res.getRes2();

            double det = getDeterminate(h1.getOrigin().getPoint(),h1.getPrevious().getOrigin().getPoint(),h2.getOrigin().getPoint());
            double det2 = getDeterminate(h2.getOrigin().getPoint(),h2.getPrevious().getOrigin().getPoint(),h1.getOrigin().getPoint());
            if(det>0||det2>0) {
                return true;
            }
        }
        return bool;
    }

    protected void createMonotonePartition(){
        Line2D.Double line;
        List<Face> monotonePartition = new ArrayList<>();
        boolean bool;
        DoubleResult res;
        for(Trapezoid trap:trapzoids.trapezoidMap){
            if(trap.bottom==null||trap.top==null) continue;
            line = isRightAndLeftPoint_on_sameLine(trap);
            if(line != null&&isDrawable(trap,line)) {
                    bool = this.hEdge.addHalfEdge(line);
                    if(bool) drawTriangle.add(line);
            }
        }


    }

    public List<Line2D.Double> getDrawTriangle() {
        return drawTriangle;
    }

    public void triangulateMonotonePolygon(HalfEdge faceEdge){
        List<HalfEdge> hedgeList = new ArrayList<>();
        HalfEdge edge = faceEdge;
        boolean bool;
        do{
            //hedgeList.add(new HalfEdge(edge));
            hedgeList.add(new HalfEdge(edge));
            edge = edge.getNext();
        }while(!(edge.getOrigin().equal(faceEdge.getOrigin())));

        if(hedgeList.size()<=3) return;

        Comparator<HalfEdge> xSortComp = new Comparator<HalfEdge>() {
            @Override
            public int compare(HalfEdge o1, HalfEdge o2) {
                return Double.compare(o1.getOrigin().getPoint().getX(),o2.getOrigin().getPoint().getX());
            }
        };

        Collections.sort(hedgeList, xSortComp);
        HalfEdge rightChain = hedgeList.get(0).getNext();
        HalfEdge leftChain = hedgeList.get(0).getPrevious();


        List<HalfEdge> stack = new ArrayList<>();
        HalfEdge v,w,x;
        stack.add(hedgeList.get(0));
        stack.add(hedgeList.get(1));

        double det;
        HalfEdge lastPopped;
        Line2D addLine;
        for(int i=2;i<hedgeList.size()-1;i++){
            v = hedgeList.get(i);
            if(isOnSameChain(stack.get(stack.size()-1), v)){
                w = pop(stack);
                lastPopped = w;
                Point2D start, end;
                boolean bool1, bool2;

                do {
                    x = stack.get(stack.size() - 1);
                    if (v.getPrevious().getOrigin().getPoint().equals(x.getOrigin().getPoint())
                            || v.getNext().getOrigin().getPoint().equals(x.getOrigin().getPoint())) {
                        break;
                    }

                    /***************************
                     * We need to check if lastPopped is a reflex Vertex. if thats the case then
                     * the new diagonal is drawable. First we need to make sure to determine that
                     * the left side is the inner of the polygon.
                     */
                    bool1 = v.getNext().getOrigin().getPoint().equals(lastPopped.getOrigin().getPoint());
                    bool2 = v.getPrevious().getOrigin().getPoint().equals(lastPopped.getOrigin().getPoint());
                    if( bool1 || bool2) {
                        if (bool1) {
                            start = v.getOrigin().getPoint();
                            end = x.getOrigin().getPoint();
                        } else {
                            end = v.getOrigin().getPoint();
                            start = x.getOrigin().getPoint();
                        }
                    } else {
                        bool1 = x.getNext().getOrigin().getPoint().equals(lastPopped.getOrigin().getPoint());

                        if (bool1) {
                            start = x.getOrigin().getPoint();
                            end = v.getOrigin().getPoint();
                        } else {
                            end = x.getOrigin().getPoint();
                            start = v.getOrigin().getPoint();
                        }
                    }

                    //check if reflex vertex
                    det = getDeterminate(lastPopped.getOrigin().getPoint(), start, end);
                    if (det<0){
                        //if we are going left it is not a reflex vertex
                        pop(stack);
                        bool = this.hEdge.addHalfEdge(new Line2D.Double(v.getOrigin().getPoint(), x.getOrigin().getPoint()));
                        if(bool){
                            drawTriangle.add(new Line2D.Double(v.getOrigin().getPoint(), x.getOrigin().getPoint()));
                        }
                        lastPopped = x;
                    }
                    else
                        break;

                }while(!stack.isEmpty());

                stack.add(lastPopped);
                stack.add(v);

            }else{
                w = null;
                while(!stack.isEmpty()){
                    w = pop(stack);
                    if(!stack.isEmpty()){
                        bool = this.hEdge.addHalfEdge(new Line2D.Double(v.getOrigin().getPoint(), w.getOrigin().getPoint()));
                        if(bool)drawTriangle.add(new Line2D.Double(v.getOrigin().getPoint(), w.getOrigin().getPoint()));
                    }
                }
                if(w!=null)
                    stack.add(w);
                stack.add(hedgeList.get(i));
            }
        }

        pop(stack);
        v = hedgeList.get(hedgeList.size()-1);
        while(true){
            if(stack.size()>1){
                w =pop(stack);
                bool = this.hEdge.addHalfEdge(new Line2D.Double(v.getOrigin().getPoint(), w.getOrigin().getPoint()));
                if(bool)drawTriangle.add(new Line2D.Double(v.getOrigin().getPoint(), w.getOrigin().getPoint()));
            }else break;
        }

    }

    /***
     * get the Determinate. If det < 0 left turn, else right turn;
     * @param center
     * @param a
     * @param b
     * @return
     */
    public static double getDeterminate(Point2D center, Point2D a, Point2D b){
        double det = (center.getX() - a.getX()) * (b.getY() - center.getY()) -
                (b.getX() - center.getX()) * (center.getY() - a.getY());

        return det;
    }

    /***
     * stack operation for an HalfEdge Stack
     * @param arrayStack
     * @return popped HalfEdge
     */
    public HalfEdge pop(List<HalfEdge> arrayStack){
        if(!arrayStack.isEmpty()) return arrayStack.remove(arrayStack.size()-1);
        else return null;
    }

    /***
     * checks if two Half edges are on the same right or left chain
     * @param stackEdge
     * @param currentEdge
     * @return
     */
    boolean isOnSameChain(HalfEdge stackEdge, HalfEdge currentEdge){
        if(stackEdge.getNext().getOrigin().equal(currentEdge.getOrigin())
                || stackEdge.getPrevious().getOrigin().equal(currentEdge.getOrigin()))
            return true;
        else return false;
    }

    public void algorithm() {
        //build_trapezoidization();
        Trapezoidalization.Seg add;
        while(!this.trapzoids.randList.isEmpty()){
            add = this.trapzoids.get_nextSeg();
            if (add!=null)
                this.trapzoids.addLineSegment(add);
        }
        createMonotonePartition();
        List<HalfEdge> eList = new ArrayList<>();
        for(Face f:this.hEdge.getFaceList()){
            eList.add(f.gethEdge());
        }
        for(HalfEdge e:eList){
            triangulateMonotonePolygon(e);
        }

    }


    public DCELStructure gethEdge() {
        return hEdge;
    }

    public static void main(String[] args) {

        Point2D a = new Point2D.Double(2,1);
        Point2D b = new Point2D.Double(5, 4);
        Point2D c = new Point2D.Double(3, 6);
        /*Line2D a = new Line2D.Double(p,q);
        Line2D b = new Line2D.Double(5,0,0,0);
        System.out.println(Triangulation.calculateAngle());*/
        System.out.println(Triangulation.getDeterminate(c,b,a));


        System.out.println();
    }
}