package Triangulation;

import Exceptions.NeighbourIntegrityException;
import Exceptions.SSNodeFalseClassException;
import DataStructures.DoubleResult;
import Exceptions.TrapezoidNotFoundException;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * creates a trapezoid map
 *
 * @author Mario Messah
 * @version 1.0
 */
public class Trapezoidalization {
    SSNode root;
    protected List<Trapezoid>  trapezoidMap;
    public List<Seg> lineSegments;
    List<Integer> randList;

    /**
     * Helper class of Line Segments
     */
    class Seg{
        Point2D a;
        Point2D b;
        boolean isInserted;
        SSNode rootA, rootB;
        int index;         //index in edgeList

        public Seg(){
        }

        public Line2D getLine(){
            return new Line2D.Double(a,b);
        }
    }

    public Trapezoidalization(List<Line2D.Double> edgeList){
        List<Line2D> eList = new ArrayList<>(edgeList);
        this.lineSegments = fill_LineSegments(eList);
        fill_randList(edgeList.size());
        initSearchStructure(get_nextSeg().getLine());

        //-------------------
        //initSearchStructure(edgeList.get(0));
        //this.lineSegments.get(0).isInserted = true;
        //----------------------------------
    }

    private SSNode getRootA(Seg a) {
        if(a.rootA==null) return this.root;
        else return a.rootA;
    }

    /**
     *
     * @param length
     */
    private void fill_randList(int length){
        ArrayList result = new ArrayList(length);
        for (int i = 0; i < length; ++i) {
            result.add(i);
        }

        Collections.shuffle(result);
        this.randList = result;
    }

    protected Seg get_nextSeg(){
        if(this.randList.size()>0) {
            int n = this.randList.remove(0);
            System.out.println("Seg: "+n);
            Seg segment = this.lineSegments.get(n);
            segment.isInserted = true;
            return segment;
        } else return null;
    }

    /**
     * Fills the Segment List
     * @param edgeList the original Edge List
     * @return
     */
    private List<Seg> fill_LineSegments(List<Line2D> edgeList){
        List<Seg> res = new ArrayList<>();
        Seg add;
        int i = 0;

        for(Line2D line : edgeList){
            add = new Seg();
            if(line.getP1().getX()<line.getP2().getX()){
                add.a = line.getP1();
                add.b = line.getP2();
            }else {
                add.a = line.getP2();
                add.b = line.getP1();
            }
            add.isInserted = false;
            //add.rootA = add.rootB = this.root;
            add.index = i;
            res.add(add);
            i++;
        }
        return res;
    }

    /**
     * Constructor. Init the Structure on creation.
     *
     *          |      /|
     *          |  2 /  |
     *      1   |  / 4  | 3
     *          |/      |
     *
     * @param firstLine make sure that p1 is the left vertex
     */
    private void initSearchStructure(Line2D firstLine){
        trapezoidMap = new ArrayList<>();

        //creating the four Trapezoids
        Trapezoid one, two, three, four;

        one = new Trapezoid();
        two = new Trapezoid();
        three = new Trapezoid();
        four = new Trapezoid();

        one.setRightP(firstLine.getP1());
        one.addRightNeighbour(two);
        one.addRightNeighbour(four);

        two.setBottom(firstLine);
        two.setLeftP(firstLine.getP1());
        two.setRightP(firstLine.getP2());
        two.addLeftNeighbour(one);
        two.addRightNeighbour(three);

        three.setLeftP(firstLine.getP2());
        three.addLeftNeighbour(two);
        three.addLeftNeighbour(four);

        four.setTop(firstLine);
        four.setLeftP(firstLine.getP1());
        four.setRightP(firstLine.getP2());
        four.addLeftNeighbour(one);
        four.addRightNeighbour(three);

        one.setIndex(0);
        trapezoidMap.add(one);
        two.setIndex(1);
        trapezoidMap.add(two);
        three.setIndex(2);
        trapezoidMap.add(four);
        four.setIndex(3);
        trapezoidMap.add(three);

        //creating the actual structure
        SSNode a = new SSNode();   //first Endpoint P1
        SSNode b = new SSNode();   //second Endpoint P2
        SSNode lineNode = new SSNode();   //second Endpoint P2

        SSNode add = new SSNode(lineNode, four);
        four.setTreeNode(add);
        lineNode.setRight(add);
        add = new SSNode(lineNode, two);
        two.setTreeNode(add);
        lineNode.setLeft(add);
        lineNode.setParent(b);
        lineNode.setKey(firstLine);

        b.setLeft(lineNode);
        add = new SSNode(b, three);
        three.setTreeNode(add);
        b.setRight(add);
        b.setParent(a);
        b.setKey(firstLine.getP2());

        a.setRight(b);
        add = new SSNode(a, one);
        one.setTreeNode(add);
        a.setLeft(add);
        a.setKey(firstLine.getP1());
        root = a;
    }



    /**
     * Calculates the slope of a given Line
     * @param line Line2D
     * @return slope of the line
     */
    private static double calc_slope(Line2D line){
        Point2D p1 = line.getP1();
        Point2D p2 = line.getP2();
        return (p2.getY()-p1.getY())/(p2.getX()-p1.getX());
    }

    /**
     * To add another line segment in the Trapezoidalization you need to find out
     * which trapezoid is the first that intersects with the added line.
     * @param a
     * @param b
     * @param cNode
     * @return corresponding Trapezoid
     */
    protected SSNode searchTrapezoid(Point2D a, Point2D b, SSNode cNode) throws SSNodeFalseClassException, TrapezoidNotFoundException{
        Line2D nodeLine = null;
        Point2D nodePoint = null;

        while (!(cNode.key instanceof Trapezoid)){
            if (cNode.key instanceof Line2D.Double){
                /* Y-Node */
                nodeLine = cNode.getLine();
                if(nodeLine.getP1().equals(a) || nodeLine.getP2().equals(a)){
                    //already inserted
                    if(Trapezoid.isAbove(nodeLine, b)){
                        cNode = cNode.left;
                    }else cNode = cNode.right;
                }
                else if(Trapezoid.isAbove(nodeLine,a)){
                    cNode = cNode.left;
                }else cNode = cNode.right;

            } else if (cNode.key instanceof Point2D.Double) {
                /* Y-Node */
                nodePoint = cNode.getPoint();
                if(a.getX()>nodePoint.getX()) cNode = cNode.right;
                else if(nodePoint.getX()==a.getX()){
                    //already inserted
                    if(b.getX()<nodePoint.getX()){
                        cNode = cNode.left;
                    }else cNode = cNode.right;
                }else cNode = cNode.left;
            }
        }
        if(cNode == null) throw new TrapezoidNotFoundException("Trapezoid is null / Not Found");
        if(!cNode.getTrapezoid().checkIfPointIsInTrapezoid(a)) throw new TrapezoidNotFoundException("Trapezoid is null / Not Found");

        return cNode;
    }


    /**
     * A test if a Trapezoid is containing a point.
     * @return
     */
    public boolean pointInTrapzoid(Trapezoid trap, Point2D p){

        if(p.getX()>trap.getLeftP().getX() && p.getX()<trap.getRightP().getX())
            if(Trapezoid.isAbove(trap.bottom,p) && !Trapezoid.isAbove(trap.getTop(), p))
                return true;
        return false;
    }

    /**
     * Given a new Line Segment and the first Trapezoid , find all trapezoids in the
     * existing Trapezoid Map, that intersect with the Line
     *
     * @param line  Line to be added
     */
    public List<Trapezoid> getIntersectedTrapezoids(Seg line){
        Trapezoid first;
        SSNode root = getRootA(line);

        //find out first Trapezoid
        try{
            first = searchTrapezoid(line.a,line.b,root).getTrapezoid();
        }catch (SSNodeFalseClassException | TrapezoidNotFoundException e){
            System.err.println(e);
            return null;
        }

        Trapezoid currentTrap = first;
        Trapezoid upper, lower;
        List<Trapezoid> trapList = new ArrayList<>();
        trapList.add(currentTrap);
        Point2D end = line.b;

        if(currentTrap.getRightP()==null)
            return trapList;

        while(end.getX()>currentTrap.rightP.getX()){
            if(currentTrap.getRightNeighbours().size()>1){

                if ((currentTrap.getRightNeighbours().get(0)).isTrapezoidAbove(currentTrap.getRightNeighbours().get(1))){
                    upper = currentTrap.getRightNeighbours().get(0);
                    lower = currentTrap.getRightNeighbours().get(1);
                }else{
                    upper = currentTrap.getRightNeighbours().get(1);
                    lower = currentTrap.getRightNeighbours().get(0);
                }

                if(Trapezoid.isAbove(line.getLine(), currentTrap.rightP)){
                    //next intersecting trapezoid is lower right
                    currentTrap = lower;
                }else currentTrap = upper;
            }else currentTrap = currentTrap.getRightNeighbours().get(0);

            trapList.add(currentTrap);

            if(currentTrap.getRightP()==null)
                return trapList;
        }
        return trapList;
    }


    private boolean isPointInserted(Seg addLine, Point2D p){
        int next,prev;

        if (addLine.index==this.lineSegments.size()-1) next=0;
        else next = addLine.index+1;
        if (addLine.index==0) prev = this.lineSegments.size()-1;
        else prev = addLine.index-1;
        Seg lineSeg;
        Line2D line;

        lineSeg = this.lineSegments.get(next);
        if(lineSeg.isInserted){
            line = lineSeg.getLine();
            if(line.getP1().equals(p) || line.getP2().equals(p)) return true;
        }
        lineSeg = this.lineSegments.get(prev);
        if(lineSeg.isInserted){
            line = lineSeg.getLine();
            if(line.getP1().equals(p) || line.getP2().equals(p)) return true;
        }
        return false;
    }

    protected void addLineSegment(Seg addLine) {

        List<Trapezoid> intersectTrap;
        boolean insertedP1 = isPointInserted(addLine, addLine.getLine().getP1());
        boolean insertedP2 = isPointInserted(addLine, addLine.getLine().getP2());

        splitPoint(insertedP1, addLine, true);
        splitPoint(insertedP2, addLine, false);

        //Step 2:
        intersectTrap = getIntersectedTrapezoids(addLine);
        List<Trapezoid> trap_before_intersection = intersectTrap.get(0).getLeftNeighbours();
        List<Trapezoid> trap_after_intersection = intersectTrap.get(intersectTrap.size()-1).getRightNeighbours();



        Trapezoid one = new Trapezoid(intersectTrap.get(0));  //above Trapezoid
        Trapezoid two = new Trapezoid(intersectTrap.get(0));  //lower Trapezoid

        /*-----------------
            check if neighbour on the right side of one or two is still valid
            in case of multi Trapezoid. deleting right neighbours if not.
        ------------------------------------*/

        if (intersectTrap.size() == 1) split_Trapezoid_line(one, two, addLine.getLine(), false);
            else split_Trapezoid_line(one, two, addLine.getLine(), true);


        boolean scissorTriggerOne = false;
        boolean scissorTriggerTwo = false;
        Trapezoid tmp;
        if(insertedP1){
            //deleting neighbours on the left side on scissor case
            if (one.top != null && one.bottom != null)
                if (one.top.getP1().equals(one.bottom.getP1())) {
                    scissorTriggerOne = true;
                    one.clearLeftNeighbours();
                }

            if (two.top != null && two.bottom != null)
                if (two.top.getP1().equals(two.bottom.getP1())) {
                    scissorTriggerTwo = true;
                    two.clearLeftNeighbours();
                }

            //fixing right neighbour of Trapezoid before intersection
            tmp = trap_before_intersection.get(0);
            if(scissorTriggerOne){
                if(tmp.rightNeighbours.size()>1){
                    tmp.rightNeighbours.remove(intersectTrap.get(0));
                    tmp.addRightNeighbour(two);
                }else tmp.addRightNeighbour(two);
            }
            if(scissorTriggerTwo){
                if(tmp.rightNeighbours.size()>1) {
                    tmp.rightNeighbours.remove(intersectTrap.get(0));
                    tmp.rightNeighbours.add(0, one);
                }else tmp.rightNeighbours.add(0, one);
            }
        }


        // correct right neighbours of trap after intersection
        if(trap_before_intersection.size()==1 && !scissorTriggerOne && !scissorTriggerTwo){
            //check
            tmp = trap_before_intersection.get(0);
            tmp.clearRightNeighbours();
            tmp.addRightNeighbour(one);
            tmp.addRightNeighbour(two);
        }else if(trap_before_intersection.size()>1){
            try {
                fill_upper_lower(trap_before_intersection);
            }catch (NeighbourIntegrityException e){
                System.err.println("Neighbour Integrity Error in Left Neighbour of Trap after Intersect");
            }

            tmp = trap_before_intersection.get(0);
            tmp.clearRightNeighbours();
            tmp.addRightNeighbour(one);

            tmp = trap_before_intersection.get(1);
            tmp.clearRightNeighbours();
            tmp.addRightNeighbour(two);
        }

        //correct left neighbours of one & two
        if(trap_before_intersection.size()>1 && insertedP1){
            one.clearLeftNeighbours();
            one.addLeftNeighbour(trap_before_intersection.get(0));

            two.clearLeftNeighbours();
            two.addLeftNeighbour(trap_before_intersection.get(1));
        }

        //**************************

        List<DoubleResult> addTrap = new ArrayList<>();
        addTrap.add(new DoubleResult(one, two));

        Trapezoid one2;
        Trapezoid two2;
        Trapezoid currentTrap;
        Trapezoid prevTrap;
        boolean flagOne = false;
        boolean flagTwo = false;
        Trapezoid upper=new Trapezoid(),lower=new Trapezoid();

        for(int i = 1;i<intersectTrap.size();i++){
            currentTrap  = intersectTrap.get(i);
            prevTrap =  intersectTrap.get(i - 1);
            one = (Trapezoid) addTrap.get(addTrap.size()-1).getRes1();
            two = (Trapezoid) addTrap.get(addTrap.size()-1).getRes2();

            one2 = new Trapezoid(intersectTrap.get(i));  //above Trapezoid
            two2 = new Trapezoid(intersectTrap.get(i));  //lower Trapezoid

            //TODO fix neighbours
            split_Trapezoid_line(one2, two2, addLine.getLine(), true);

            if(one.getRightP()==null){
                //merge one
                one.setRightP(one2.getRightP());
                one.setRightNeighbours(one2.getRightNeighbours());

                //one2.setLeftP(one.getLeftP());
                //one2.setLeftNeighbours(one.getLeftNeighbours());
                one2 = one;
                flagOne = true;
            }
            else if(two.getRightP()==null){
                //merge two
                two.setRightP(two2.getRightP());
                two.setRightNeighbours(two2.getRightNeighbours());
                //two2.setLeftP(two.getLeftP());
                //two2.setLeftNeighbours(two.getLeftNeighbours());
                two2 = two;
                flagTwo = true;
            }

            //fixing right neighbour of one & two
            addTrapLoopFunc(currentTrap,one, one2, prevTrap, flagOne);
            addTrapLoopFunc(currentTrap, two, two2, prevTrap, flagTwo);

            //if(one.equalss(one2)) one2 = one;
            //if(two.equalss(two2)) two2 = two;
            //fix right neighbour of one2 & two2
            if(i+1>=intersectTrap.size()){
                if(trap_after_intersection.size()==1){
                    one2.clearRightNeighbours();
                    one2.addRightNeighbour(trap_after_intersection.get(0));
                    two2.clearRightNeighbours();
                    two2.addRightNeighbour(trap_after_intersection.get(0));
                }else if(trap_after_intersection.size()>1){
                    try {
                        fill_upper_lower(trap_after_intersection);
                    }catch (NeighbourIntegrityException e){
                        System.err.println("Neighbour Integrity Error in Left Neighbour of Trap after Intersect");
                    }
                    one2.clearRightNeighbours();
                    one2.addRightNeighbour(trap_after_intersection.get(0));
                    two2.clearRightNeighbours();
                    two2.addRightNeighbour(trap_after_intersection.get(1));
                }
                flagOne = false;
                flagTwo = false;
            }

            //update
            DoubleResult add = new DoubleResult(one2, two2);
            addTrap.add(add);

        }


        //Fix neighbour of first Trapezoid before intersection

        //Fixing neighbours
        DoubleResult lastDResult;
        if(addTrap.size()>1) {
            lastDResult = addTrap.get(addTrap.size() - 1);
        }else lastDResult=addTrap.get(0);

        one = (Trapezoid)lastDResult.getRes1();
        two = (Trapezoid)lastDResult.getRes2();

        scissorTriggerOne = false;
        scissorTriggerTwo = false;

        if(insertedP2){
            //deleting neighbours on the left side on scissor case
            if (one.top != null && one.bottom != null)
                if (one.top.getP2().equals(one.bottom.getP2())) {
                    scissorTriggerOne = true;
                    one.clearRightNeighbours();
                }

            if (two.top != null && two.bottom != null)
                if (two.top.getP2().equals(two.bottom.getP2())) {
                    scissorTriggerTwo = true;
                    two.clearRightNeighbours();
                }

            //fixing left neighbour of Trapezoid before intersection
            tmp = trap_after_intersection.get(0);
            if(scissorTriggerOne){
                if(tmp.leftNeighbours.size()>1){
                    tmp.leftNeighbours.remove(intersectTrap.get(intersectTrap.size()-1));
                    tmp.addLeftNeighbour(two);
                }else tmp.addLeftNeighbour(two);
            }
            if(scissorTriggerTwo){
                if(tmp.leftNeighbours.size()>1){
                    tmp.rightNeighbours.remove(intersectTrap.get(intersectTrap.size()-1));
                    tmp.addLeftNeighbour(one);
                }else tmp.addRightNeighbour(one);
            }
        }

        // correct left neighbour of trap after intersection
        if(trap_after_intersection.size()==1 && !scissorTriggerOne && !scissorTriggerTwo){
            //check
            tmp = trap_after_intersection.get(0);
            tmp.clearLeftNeighbours();
            tmp.addLeftNeighbour(one);
            tmp.addLeftNeighbour(two);
        }else if(trap_after_intersection.size()>1){
            try {
                fill_upper_lower(trap_after_intersection);
            }catch (NeighbourIntegrityException e){
                System.err.println("Neighbour Integrity Error in Left Neighbour of Trap after Intersect");
            }

            tmp = trap_after_intersection.get(0);
            tmp.clearLeftNeighbours();
            tmp.addLeftNeighbour(one);

            tmp = trap_after_intersection.get(1);
            tmp.clearLeftNeighbours();
            tmp.addLeftNeighbour(two);
        }

        //correcting last one & two already happened

        /*---------------end fix neighbour------------------------*/

        //update Tree & trapezoid map
        SSNode root, lineRoot;
        int counterflag=0;
        DoubleResult doubleTrap;
        Trapezoid nextTrap;
        SSNode node1 = new SSNode();
        SSNode node2 = new SSNode();
        int indexNext = 1;

        for(int i = 0;i<intersectTrap.size();i++){
            currentTrap = intersectTrap.get(i);
            root = currentTrap.getTreeNode();
            doubleTrap = addTrap.get(i);

            //in case of unregistered left neigh of one or two


            int n = -1;

            if(i>0){

                //update Map
                if(!isTrapezoid_in_DoubleResult_equal(addTrap.get(i-1), doubleTrap, true)) {
                    ((Trapezoid) doubleTrap.getRes1()).deleteEmptyNeighbours();
                    updateTrapezoidHelper(currentTrap, (Trapezoid) doubleTrap.getRes1(), (Trapezoid) doubleTrap.getRes2(), true, true);
                }
                if(!isTrapezoid_in_DoubleResult_equal(addTrap.get(i-1), doubleTrap, false)){
                    ((Trapezoid) doubleTrap.getRes2()).deleteEmptyNeighbours();
                    updateTrapezoidHelper(currentTrap, (Trapezoid)doubleTrap.getRes1(), (Trapezoid)doubleTrap.getRes2(), false, true);
                }
            }else{
                if(intersectTrap.size()==1)
                    updateTrapezoidHelper(currentTrap, (Trapezoid)doubleTrap.getRes1(), (Trapezoid)doubleTrap.getRes2(),
                            null, true);
                else updateTrapezoidHelper(currentTrap, (Trapezoid)doubleTrap.getRes1(), (Trapezoid)doubleTrap.getRes2(), null, true);
            }

            if(i!=0){
                if(!addTrap.get(i-1).getRes1().equals(doubleTrap.getRes1()))
                    node1 = new SSNode();
            }else node1 = new SSNode();
            node1.setKey(doubleTrap.getRes1());
            node1.setParent(root);
            ((Trapezoid) doubleTrap.getRes1()).setTreeNode(node1);

            if(i!=0){
                if(!addTrap.get(i-1).getRes2().equals(doubleTrap.getRes2()))
                    node2 = new SSNode();
            }else node2 = new SSNode();
            node2.setKey(doubleTrap.getRes2());
            node2.setParent(root);
            ((Trapezoid) doubleTrap.getRes2()).setTreeNode(node2);

            root.setKey(addLine.getLine());
            root.setLeft(node1);
            root.setRight(node2);

            unregister_Trap(currentTrap);
            currentTrap.setToNull();
        }

        addLine.isInserted=true;
        try {
            checkNeighbourIntegrity();
        }catch (NeighbourIntegrityException e){
            System.err.println(e);
        }
    }

    public void addTrapLoopFunc(Trapezoid currentTrap, Trapezoid one, Trapezoid one2, Trapezoid prevTrap, boolean flag){
        //fix right neighbours in case of right P null through merging
        if (one.getRightP()!=null && !flag){
            deleteTrapFromMap(currentTrap, one.getRightNeighbours());
            one.addRightNeighbour(one2);

            deleteTrapFromMap(prevTrap,one2.getLeftNeighbours());
            one2.addLeftNeighbour(one);
        }else if(one.rightP!=null && one2.rightP!=null){
            if(one.rightP.equals(one2.leftP)) {
                deleteTrapFromMap(currentTrap, one.getRightNeighbours());
                one.addRightNeighbour(one2);
                deleteTrapFromMap(prevTrap, one2.getLeftNeighbours());
                one2.addLeftNeighbour(one);
            }
        }
    }

    private boolean isTrapezoid_in_DoubleResult_equal(DoubleResult a, DoubleResult b, Boolean mode){
        Trapezoid one, two;

        if(mode==true){
            one = (Trapezoid) a.getRes1();
            two = (Trapezoid) b.getRes1();
        }else{
            one = (Trapezoid) a.getRes2();
            two = (Trapezoid) b.getRes2();
        }

        if(one.equalss(two)) {
            return true;
        }else return false;

    }


    /**
     * Sort a neighbour Array. The first Trapezoid is above the second one.
     * @param neigh Neighbour Array of Trapezoids
     * @throws NeighbourIntegrityException checks if the Array is correct
     */
    public void fill_upper_lower(List<Trapezoid> neigh) throws NeighbourIntegrityException {
        if(neigh.size()>2) throw new NeighbourIntegrityException("");
        Trapezoid tmp;
        if(neigh.get(1).isTrapezoidAbove(neigh.get(0))){
            tmp =  neigh.get(0);
            neigh.remove(0);
            neigh.add(tmp);
        }
    }

    public int deleteTrapFromMap(Trapezoid delete, List<Trapezoid> trapMap){
        int n = trapMap.indexOf(delete);
        Trapezoid del;
        if(n>=0){
            del = trapMap.remove(n);
        } else del=null;
        return n;
    }



    void updateTrapezoidHelper(Trapezoid delete, Trapezoid one, Trapezoid two, Boolean addBool, boolean updateBool){

        int n = deleteTrapFromMap(delete, this.trapezoidMap);
        Trapezoid del = delete;

        if(addBool == null){
            trapezoidMap.add(one);
            if (updateBool) updateNeighbours_after_Insertion(one, del, true, true);
            trapezoidMap.add(two);
            if (updateBool) updateNeighbours_after_Insertion(two, del, true, true);

        }else if(addBool == true){
            trapezoidMap.add(one);
            if (updateBool) updateNeighbours_after_Insertion(one, del, true, true);
        }else{
            trapezoidMap.add(two);
            if (updateBool) updateNeighbours_after_Insertion(two, del, true, true);
        }
    }


    public void unregister_Trap(Trapezoid del){
        List<Trapezoid> leftneigh;
        List<Trapezoid> rightneigh;
        Trapezoid tmpTrap;

        if(del.getRightNeighbours()!=null){
            rightneigh = del.getRightNeighbours();
            for(int i=0;i<rightneigh.size();i++){
                tmpTrap = rightneigh.get(i);
                tmpTrap.getLeftNeighbours().remove(del);
            }
        }
        if(del.getLeftNeighbours()!=null){
            rightneigh = del.getLeftNeighbours();
            for(int i=0;i<rightneigh.size();i++){
                tmpTrap = rightneigh.get(i);
                tmpTrap.getRightNeighbours().remove(del);
            }
        }
    }

    /*****************
     *
     * @param trap
     * @param del
     * @param leftSide
     * @param rightSide
     */
    private void updateNeighbours_after_Insertion(Trapezoid trap, Trapezoid del, boolean leftSide, boolean rightSide){
        List<Trapezoid> leftneigh;
        List<Trapezoid> rightneigh;
        Trapezoid tmpTrap;
        int n;

        if(trap.getRightNeighbours()!=null && rightSide){
            rightneigh = trap.getRightNeighbours();
            for(int i=0;i<rightneigh.size();i++){
                tmpTrap = rightneigh.get(i);
                if(tmpTrap.isEmpty()){
                    rightneigh.remove(tmpTrap);
                    continue;
                }
                n = tmpTrap.getLeftNeighbours().indexOf(trap);
                if(n<0){
                    tmpTrap.addLeftNeighbour(trap);
                    n = tmpTrap.getLeftNeighbours().indexOf(del);
                    if(n>=0) tmpTrap.getLeftNeighbours().remove(n);
                }
            }
        }
        if(trap.getLeftNeighbours()!=null && leftSide){
            leftneigh = trap.getLeftNeighbours();
            for(int i=0;i<leftneigh.size();i++){
                tmpTrap = leftneigh.get(i);
                if(tmpTrap.isEmpty()){
                    leftneigh.remove(tmpTrap);
                    continue;
                }
                n = tmpTrap.getRightNeighbours().indexOf(trap);
                if(n<0){
                    tmpTrap.addRightNeighbour(trap);
                    n = tmpTrap.getRightNeighbours().indexOf(del);
                    if(n>=0) tmpTrap.getRightNeighbours().remove(n);
                }
            }
        }
    }

    private void splitPoint(boolean inserted, Seg addLine, boolean isFirstPoint){
        SSNode beginNode=null, trapNode = null;
        Trapezoid first=null, one=null, two=null;

        if(!inserted) {
            try {
                boolean searchbool=false;
                if(isFirstPoint){
                    if(addLine.rootA==null)searchbool = true;
                }else{
                    if(addLine.rootB==null)searchbool = true;
                }

                if(searchbool) {
                    if (isFirstPoint) {
                        trapNode = searchTrapezoid(addLine.getLine().getP1(), addLine.getLine().getP2(), this.root);
                        addLine.rootA = trapNode;
                    }
                    else{
                        trapNode = searchTrapezoid(addLine.getLine().getP2(), addLine.getLine().getP1(), this.root);
                        addLine.rootB = trapNode;
                    }
                }else{
                    if (isFirstPoint) {
                        trapNode = addLine.rootA;
                    }
                    else{
                        trapNode = addLine.rootB;
                    }
                }

                /* split first in 2 Trapezoids */
                first = trapNode.getTrapezoid();
                one = new Trapezoid(first);
                two = new Trapezoid(first);
                if(isFirstPoint)split_Trapezoid_vertical(one, two, addLine.getLine().getP1());
                else split_Trapezoid_vertical(one, two, addLine.getLine().getP2());
            } catch (SSNodeFalseClassException | TrapezoidNotFoundException e) {
                System.err.println(e);
                return;
            }

            //fix right neighbour
            updateTrapezoidHelper(first, one, two, null, true);


            if(isFirstPoint)split_Trapezoid_Tree(one, two, addLine.getLine().getP1(), first.getTreeNode()); //update tree
            else split_Trapezoid_Tree(one, two, addLine.getLine().getP2(), first.getTreeNode());
        }
    }


    private void checkNeighbourIntegrity() throws NeighbourIntegrityException {
        List<Trapezoid> leftN;
        List<Trapezoid> rightN;
        for(Trapezoid trap:this.trapezoidMap){
            rightN = trap.getRightNeighbours();
            if(rightN!=null){
                if(rightN.size()>2)throw new NeighbourIntegrityException("Neighbours wrong");
                for(Trapezoid trap2:rightN){
                    if(!trap2.getLeftNeighbours().contains(trap))
                        throw new NeighbourIntegrityException("Neighbours wrong");
                }
            }

            leftN = trap.getLeftNeighbours();
            if(leftN!=null){
                if(leftN.size()>2)throw new NeighbourIntegrityException("Neighbours wrong");
                for(Trapezoid trap2:leftN){
                    if(!trap2.getRightNeighbours().contains(trap))
                        throw new NeighbourIntegrityException("Neighbours wrong");
                }
            }
        }

    }

    private void split_Trapezoid_line(Trapezoid one, Trapezoid two, Line2D line, boolean mode){
        one.setBottom(line);
        two.setTop(line);
        if(mode) {
            if(one.getRightP()!=null && !one.getRightP().equals(line.getP2())) {
                if (Trapezoid.isAbove(line, one.getRightP())) {
                    two.setRightP(null);
                    two.clearRightNeighbours();
                } else {
                    one.setRightP(null);
                    one.clearRightNeighbours();
                }
            }
        }
    }

    /**
     *  Splits a Trapezoid into two. Trapezoids inserted must be a full
     *  copy of the old Trapezoid.
     * @param one   full copy of the old Trapezoid
     * @param two   full copy of the old Trapezoid
     * @param p
     */
    private void split_Trapezoid_vertical(Trapezoid one, Trapezoid two, Point2D p){

        one.setRightP(p);
        one.clearRightNeighbours();
        one.addRightNeighbour(two);

        two.setLeftP(p);
        two.clearLeftNeighbours();
        two.addLeftNeighbour(one);
    }

    private void split_Trapezoid_Tree(Trapezoid one, Trapezoid two, Point2D p, SSNode oldNode){
        SSNode nodeOne = new SSNode();
        SSNode nodeTwo = new SSNode();
        SSNode nodeParent = new SSNode();

        nodeOne.setKey(one);
        nodeOne.setParent(nodeParent);
        one.setTreeNode(nodeOne);

        nodeTwo.setKey(two);
        nodeTwo.setParent(nodeParent);
        two.setTreeNode(nodeTwo);

        oldNode.setLeft(nodeOne);
        oldNode.setRight(nodeTwo);
        oldNode.setKey(p);
    }





    /**********************
     * test methods
     **********************/

    public static void main(String [ ] args){
        //TODO test search loop
        Point2D a = new Point2D.Double(1,1.5);
        Point2D b = new Point2D.Double(2,2.5);
        Line2D line = new Line2D.Double(a,b);
        Line2D line2 = new Line2D.Double(2,2.5,3, 1.5);
        Line2D line3 = new Line2D.Double(2.5,1,3, 1.5);
        Line2D line4 = new Line2D.Double(1,1.5,2.5,1);


        /*Line2D line2 = new Line2D.Double(1,1,4, 2);
        Line2D line3 = new Line2D.Double(2.5,3.5,4.5, 5);
        Line2D line4 = new Line2D.Double(0.5,2,5,3);
    */
        /*Point2D p1 = new Point2D.Double(2,4);
        Point2D p2 = new Point2D.Double(3,1);

        List<Point2D> blubb = new ArrayList<>();

        //blubb.add(a);
        //blubb.add(b);
        blubb.add(p1);
        blubb.add(p2);

        List<Line2D> eList = new ArrayList<>();


        eList.add(line);
        eList.add(line2);
        eList.add(line3);
        eList.add(line4);


        Trapezoidalization struct = new Trapezoidalization(eList);
        //SSNode res = (SSNode) struct.searchFirstTrapezoid(new Line2D.Double(1.5, 2.5,4,4)).getRes1();


        //List<Trapezoid> intersect = struct.getIntersectedTrapezoids(struct.get_nextSeg());

        struct.addLineSegment(struct.lineSegments.get(2));
        struct.addLineSegment(struct.lineSegments.get(1));
        struct.addLineSegment(struct.lineSegments.get(3));

        //struct.addLineSegment(struct.lineSegments.get(1));

        //struct.addLineSegment(struct.get_nextSeg());
        //struct.addLineSegment(struct.get_nextSeg());
        //struct.addLineSegment(struct.get_nextSeg());
        //struct.addLineSegment(struct.get_nextSeg());

        System.out.println(isAbove(line2,p1));
        //System.out.println(isAbove(line,p2));
//        System.out.println(struct.trapezoidMap.get(1).getRightNeighbours().get(1).isEmpty());*/
    }

}
