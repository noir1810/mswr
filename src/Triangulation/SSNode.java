package Triangulation;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import Exceptions.SSNodeFalseClassException;

/**
 * Node for Trapezoidalization
 *
 * @author Mario Messah
 * @version 1.0
 */

public class SSNode {

    Object parent;
    SSNode left;
    SSNode right;
    Object key;                     //Either X-node (Point2D) or Y-Node (Line2D) or leaf (Trapzoid)

    public SSNode(){
    }

    public SSNode(SSNode parent, Object key){
        this.parent = parent;
        this.key = key;
    }

    public Line2D getLine() throws SSNodeFalseClassException{
        if(!(key instanceof Line2D))
            throw new SSNodeFalseClassException("Error in SSNode. Key is not Line2D");
        else
            return (Line2D.Double) key;
    }

    public Point2D getPoint() throws SSNodeFalseClassException{
        if(!(key instanceof Point2D))
            throw new SSNodeFalseClassException("Error in SSNode. Key is not Point2D");
        else
            return (Point2D) key;
    }

    public Trapezoid getTrapezoid()throws SSNodeFalseClassException{
        if(!(key instanceof Trapezoid))
            throw new SSNodeFalseClassException("Error in SSNode. Key is not Trapezoid");
        else
            return (Trapezoid) key;
    }

    public void setLeft(SSNode left) {
        this.left = left;
    }

    public void setKey(Object key) {
        this.key = key;
    }

    public void setParent(SSNode parent) {
        this.parent = parent;
    }

    public void setParent(Object parent) {
        this.parent = parent;
    }

    public Object getParent() {
        return parent;
    }

    public void setRight(SSNode right) {
        this.right = right;
    }
}
