package DataStructures;

import DoubleConnectedEdge.DCELStructure;
import MSWRPolygons.LineHelper;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Polygon implements Serializable{
    public List<Line2D.Double> edgeList = new ArrayList<>();
    public List<Point2D.Double> vertexList= new ArrayList<>();
    private List<Line2D.Double> cuttingList = new ArrayList<>();
    private List<Line2D.Double> essentialList = new ArrayList<>();
    private List<Line2D.Double> mswr = new ArrayList<>();
    private List<Line2D.Double> triangulationEdges = new ArrayList<>();
    protected DCELStructure hEdgeStruct;

    public Polygon(){}

    public Polygon(List<Point2D.Double> vertexList){
        this.vertexList = vertexList;
        initEdgeArray(vertexList);
        this.hEdgeStruct = new DCELStructure(this);
    }



    public void setCuttingList(List<Line2D.Double> cuttingList) {
        this.cuttingList = new ArrayList<>(cuttingList);
    }

    public void setMswr(List<Line2D.Double> mswr) {
        this.mswr = mswr;
    }

    public void setEssentialList(List<Line2D.Double> essentialList) {
        this.essentialList = new ArrayList<>(essentialList);
    }

    public List<Line2D.Double> getEssentialList() {
        return essentialList;
    }

    public List<Line2D.Double> getMswr() {
        return mswr;
    }


    public List<Line2D.Double> getCuttingList() {
        return cuttingList;
    }

    public boolean isEmpty(){
        return edgeList.size() <= 0;
    }

    public boolean routeIsEmpty(){
        return mswr.size() <= 0;
    }

    public List<Line2D.Double> getEdgeList(){
            return edgeList;
    }

    public List<Point2D.Double> getVertexList() {
        return vertexList;
    }

    public void setEdgeList(List<Line2D.Double> edgeList) {
        this.edgeList = edgeList;
    }

    public void setVertexList(List<Point2D.Double> vertexList) {
        this.vertexList = vertexList;
    }

    public void initEdgeArray(List<Point2D.Double> vertexArr) {
        ArrayList<Line2D.Double> edgeArr=new ArrayList<>();
        Point2D.Double p1,p2;

        for (int i = 0; i < vertexArr.size()-1; i++){
            p1 = vertexArr.get(i);
            p2 = vertexArr.get(i+1);

            edgeArr.add(new Line2D.Double(p2,p1));
        }

        edgeArr.add(new Line2D.Double(vertexArr.get(0),vertexArr.get(vertexArr.size()-1)));

        setEdgeList(edgeArr);
    }

    public void addEdge(Point2D.Double start, Point2D.Double end) {
        Line2D.Double e = new Line2D.Double(start, end);
        edgeList.add(e);
    }

    public void addVertex(Point2D.Double v) {
        this.vertexList.add(v);
    }

    public List<Line2D.Double> getTriangulationEdges() {
        return triangulationEdges;
    }

    public void setTriangulationEdges(List<Line2D.Double> triangulationEdges) {
        this.triangulationEdges = triangulationEdges;
    }

    private void printEdgeArray(){
        System.out.println("Edge array");
        //  Add the Edge to the List so it can be repainted
        for(int i=0;i<=edgeList.size()-1;i++){
            System.out.println(edgeList.get(i).getP1()+" "+edgeList.get(i).getP2());
        }
    }

    public static double lineDist(Line2D.Double l){
        double tmp = Math.pow(l.getX2()-l.getX1(),2) + Math.pow(l.getY2()-l.getY1(),2);
        return Math.sqrt(tmp);
    }

    public double getDistRoute(List<Line2D.Double> route){
        double dist = 0;

        for(Line2D.Double l : route){
            dist += lineDist(l);
        }
        return dist;
    }

    public double getDelay(){
        double min=Double.POSITIVE_INFINITY;
        for(Line2D.Double l : this.getEssentialList()){
            if(lineDist(l)<min) min = lineDist(l);
        }
        return getDistRoute(this.getMswr())-min;
    }



    @Override
    public String toString() {
        return super.toString();
    }
}
