package DataStructures;

import java.awt.geom.Line2D;

public class LineContainer {
    private Line2D.Double line;
    private int startIndex;
    private int endIndex;
    private boolean deletable;

    public LineContainer(){
    }

    public LineContainer(Line2D.Double line, int startIndex, int endIndex){
        this.line=line;
        this.startIndex=startIndex;
        this.endIndex=endIndex;
        this.deletable = false;
    }

    public void setDeletable(boolean deletable) {
        this.deletable = deletable;
    }

    public void setLine(Line2D.Double line) {
        this.line = line;
    }

    public void setStartIndex(int index) {
        this.startIndex = index;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public Line2D.Double getLine() {
        return line;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public static boolean lineEqual(Line2D.Double a, Line2D.Double b){
        if(a.getX1()==b.getX1()&&a.getX2()==b.getX2()&&a.getY1()==b.getY1()&&a.getY2()==b.getY2()){
            return true;
        }else return false;
    }

    public boolean equal(LineContainer a){
        if(lineEqual(this.line, a.getLine())&& this.startIndex==a.startIndex){
            return true;
        }
        return false;
    }
}
