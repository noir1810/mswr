package DataStructures;

public class DoubleResult<T>{
    private  T res1;
    private  T res2;

    public DoubleResult(T first, T second) {
        this.res1 = first;
        this.res2 = second;
    }

    public DoubleResult() {
    }

    public void setResult(T res1, T res2){
        this.res1 = res1;
        this.res2 = res2;
    }

    public void setRes1(T res1){
        this.res1 = res1;
    }

    public void setRes2(T res2){
        this.res2 = res2;
    }

    public T getRes1() {
        return res1;
    }

    public T getRes2() {
        return res2;
    }

}
