package MSWRPolygons;

import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;

public class LineHelper {
    Line2D.Double line;
    boolean bool;

    public LineHelper(){}

    public LineHelper(Line2D.Double line, boolean bool){
        this.line = line;
        this.bool = bool;
    }

    public void setLine(Line2D.Double line) {
        this.line = line;
    }

    public Line2D.Double getLine() {
        return line;
    }

    public void setBool(boolean bool) {
        this.bool = bool;
    }

    public boolean getBool() {
        return this.bool;
    }

    public static List<Line2D.Double> getHelperArrayAsLines(List<LineHelper> arr){
        List<Line2D.Double> res = new ArrayList<>();

        for(LineHelper helper:arr){
            res.add(helper.getLine());
        }
        return res;
    }
}
