package MSWRPolygons;

import DataStructures.Polygon;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.*;
import java.util.*;
import java.util.List;


public class GUI{


    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    private static void createAndShowGUI(){
        DrawingArea drawingArea = new DrawingArea();
        ButtonPanel buttonPanel = new ButtonPanel(drawingArea);

        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("MSWR");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        MenuPanel menuPanel = new MenuPanel(frame, drawingArea);
        frame.getContentPane().add(drawingArea);
        frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);

        frame.setSize(600, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

    //================================================================================
    // Buttonpanel
    //================================================================================
    static class ButtonPanel extends JPanel implements ActionListener {
        private DrawingArea drawingArea;
        //EssentialVertex essential = new EssentialVertex();
        //Object ob = new Object();

        public ButtonPanel(DrawingArea drawingArea) {
            this.drawingArea = drawingArea;

            /*add(createButton("	", Color.BLACK));
            add(createButton("	", Color.RED));
            add(createButton("	", Color.GREEN));
            add(createButton("	", Color.BLUE));*/
            add(createButton("Delete All", null));
            add(createButton("MSWR", null));
            //add(createButton("Step", null));

        }

        private JButton createButton(String text, Color background) {
            JButton button = new JButton(text);
            button.setBackground(background);
            button.addActionListener(this);

            return button;
        }

        public void actionPerformed(ActionEvent e) {
            JButton button = (JButton) e.getSource();


            if ("Delete All".equals(e.getActionCommand()))
                drawingArea.clear();
            if ("MSWR".equals(e.getActionCommand())) {
                RSP_Algorithm_L1 alg = new RSP_Algorithm_L1(drawingArea.getPolygon());
                alg.algorithmMswr();

                drawingArea.setPolygon(alg.p);
                drawingArea.repaint();
            }

        }

    }


    //================================================================================
    //Menupanel
    //================================================================================
    public static class MenuPanel extends JFrame {
        JFrame frame;

        JMenuBar menuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenu edit = new JMenu("Edit");
        JMenu about = new JMenu("About");
        //JMenuItem add_circle = new JMenuItem("Add Circle");
        JMenuItem save = new JMenuItem("Save Constellation");
        JMenuItem load = new JMenuItem("Load Constellation");
        JMenuItem ab_prgm = new JMenuItem("About the program");
        JMenuItem viewToogle = new JMenuItem("Toogle View");
        //JMenuItem showEssential = new JMenuItem("Show Essential Lines");



        public MenuPanel(JFrame frame, final DrawingArea drawingArea) {
            menuBar.add(file);
            menuBar.add(edit);
            menuBar.add(about);

            //file.add(add_circle);
            file.add(save);
            file.add(load);
            edit.add(viewToogle);
            //edit.add(showEssential);
            about.add(ab_prgm);

            frame.setJMenuBar(menuBar);

            viewToogle.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    JCheckBox   cutting = new JCheckBox("Show cutting Lines");
                    JCheckBox   essential = new JCheckBox("Show essential Lines");
                    JCheckBox   tria = new JCheckBox("Show Triangulation");
                    JCheckBox   mswr = new JCheckBox("Show MSWR");

                    String msg = "Toogle view";
                    Object[] msgContent = {msg, cutting, essential, tria, mswr};

                    int n = JOptionPane.showConfirmDialog ( null,  msgContent,
                            "Title", JOptionPane.OK_CANCEL_OPTION);

                    boolean cutRemember = cutting.isSelected();
                    boolean essentialRemember = essential.isSelected();
                    boolean triaRemember = tria.isSelected();
                    boolean mswrRemember = mswr.isSelected();

                    if(!cutRemember){
                        drawingArea.changePaintArr(1,false);
                    }else drawingArea.changePaintArr(1,true);

                    if(!essentialRemember){
                        drawingArea.changePaintArr(2,false);
                    }else drawingArea.changePaintArr(2,true);

                    if(!triaRemember){
                        drawingArea.changePaintArr(3,false);
                    }else drawingArea.changePaintArr(3,true);

                    if(!mswrRemember){
                        drawingArea.changePaintArr(4,false);
                    }else drawingArea.changePaintArr(4,true);

                }

            });

            ab_prgm.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    JOptionPane.showMessageDialog(null,"This program finds " +
                            "the maximum  shortest  watchman  route\n\n" +
                            "Draw the polygon yourself clockwise. A small circle will appear if you start drawing. You have to end " +
                            "the polygon here. Avoid Vertices with an identical x-Coordinate. Hit MSWR Button.\n\n" +
                            "Toggle the view on the menu panel via Edit. You can also show the Triangulation or essential Lines here.");
                }

            });

            save.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    JFileChooser chooser = new JFileChooser();
                    chooser.setCurrentDirectory(new java.io.File("."));
                    chooser.setDialogTitle("Save Polygon");


                    chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                    chooser.setAcceptAllFileFilterUsed(false);


                    chooser.setDialogType(JFileChooser.SAVE_DIALOG);
                    chooser.setSelectedFile(new File("myfile.osr"));
                    chooser.setFileFilter(new FileNameExtensionFilter("osr file","osr"));

                    chooser.setAcceptAllFileFilterUsed(false);

                    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                        String path=chooser.getSelectedFile().getAbsolutePath();

                        //String filename = chooser.getSelectedFile().toString();
                        if (!path .endsWith(".osr"))
                            path += ".osr";

                        try {
                            FileOutputStream fileOut;
                            String current = new java.io.File( "." ).getCanonicalPath();
                            fileOut = new FileOutputStream(path);
                            ObjectOutputStream out = new ObjectOutputStream(fileOut);
                            out.writeObject(drawingArea.getPolygon().getVertexList());

                            //out.writeObject(drawingArea.getPolygon());
                            out.close();
                            fileOut.close();
                            System.out.printf("Serialized data is saved in "+path);
                        }catch(IOException i) {
                            i.printStackTrace();
                        }
                    } else {
                        System.out.println("No Selection ");
                    }
                }
            });

            load.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    JFileChooser chooser = new JFileChooser();
                    chooser.setCurrentDirectory(new java.io.File("."));
                    chooser.setDialogTitle("Load Polygon");
                    chooser.setDialogType(JFileChooser.OPEN_DIALOG);
                    chooser.setFileFilter(new FileNameExtensionFilter("osr file","osr"));
                    //chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    //chooser.setAcceptAllFileFilterUsed(false);

                    chooser.setAcceptAllFileFilterUsed(false);

                    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {

                        Polygon p = null;
                        ArrayList<Line2D.Double> edgeArray = null;
                        try {
                            FileInputStream fileIn = new FileInputStream(chooser.getSelectedFile());
                            ObjectInputStream in = new ObjectInputStream(fileIn);
                            //p = (Polygon) in.readObject();
                            p = new Polygon((List<Point2D.Double>) in.readObject());
                            in.close();
                            fileIn.close();
                            drawingArea.setPolygon(p);
                        }catch(IOException i) {
                            i.printStackTrace();
                            return;
                        }catch(ClassNotFoundException c) {
                            System.out.println("Polygon not found");
                            c.printStackTrace();
                            return;
                        }
                    } else {
                        System.out.println("No Selection ");
                    }
                }
            });
        }

        public double[] create_dialog(){
            // Erstellung Array vom Datentyp Object, Hinzufügen der Komponenten
            JTextField xcenter = new JTextField();
            JTextField ycenter = new JTextField();
            JTextField radius = new JTextField();
            JTextField order_number = new JTextField();

            double[]input = new double[ 4 ];
            Object[] message = {"X-Center", xcenter,
                    "Y-Center", ycenter, "Radius", radius,"Order number", order_number};

            JOptionPane pane = new JOptionPane( message,
                    JOptionPane.PLAIN_MESSAGE,
                    JOptionPane.OK_CANCEL_OPTION);
            pane.createDialog(null, "Input Circle Data").setVisible(true);

            //System.out.println("Eingabe: " + xcenter.getText() + ", " + ycenter.getText());
            input[0] = Double.parseDouble(xcenter.getText());
            input[1] = Double.parseDouble(ycenter.getText());
            input[2] = Double.parseDouble(radius.getText());
            input[3] = Double.parseDouble(order_number.getText());
            return input;
        }


    }
}
