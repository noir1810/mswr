package MSWRPolygons;

import DataStructures.Polygon;
import DoubleConnectedEdge.DCELStructure;
import DoubleConnectedEdge.HalfEdge;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.event.MouseInputAdapter;


/**
 * Drawing Area
 *
 * @author Mario Messah
 */
public class DrawingArea extends JPanel implements java.io.Serializable{

    private transient final static int AREA_SIZE = 600;
    private transient Polygon p = new Polygon();

    // edges, cuttinglines, essentialLines, triangulation, mswr
    private boolean[] paintArr={true,false,false,false, true};
    private transient Line2D.Double  line= null;
    private transient Ellipse2D.Double c = null;
    MyMouseListener ml;

    //Constructor
    public DrawingArea() {
        setBackground(Color.WHITE);

        ml = new MyMouseListener();
        addMouseListener(ml);
        addMouseMotionListener(ml);
    }

    public void changePaintArr(int index, boolean value){
        this.paintArr[index]=value;
    }

    @Override
    public Dimension getPreferredSize() {
        return isPreferredSizeSet() ?
                super.getPreferredSize() : new Dimension(AREA_SIZE, AREA_SIZE);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        List<Line2D.Double> edges = new ArrayList<>();

        if(!p.isEmpty()){
            //edges=p.getEdgeList();
        }


        //paint Edges
        if (paintArr[0]) {
            if (p.vertexList.size()>0){
                DCELStructure half = new DCELStructure(p);
                paintDCELPoly(g, Color.BLACK, half);
            }
            //paintLineArray(g, Color.black,edges);
            if (line!=null){
                Graphics2D g1 = (Graphics2D) g;
                g1.setColor(Color.black);
                g1.draw(line);
            }
            if (c!=null){
                Graphics2D g2 = (Graphics2D) g;
                g2.setColor(Color.black);
                g2.draw(c);

            }
        }

        //Paint cutting Lines
        if(p.getCuttingList()!=null && paintArr[1]){
            paintLineArray(g, Color.BLACK,p.getCuttingList());
        }

        //paint Essentials
        if(paintArr[2]&&p.getEssentialList()!=null){
            paintLineArray(g, Color.red,p.getEssentialList());
        }

        //paint Triangulation
        if(paintArr[3]&&p.getTriangulationEdges()!=null){
            paintLineArray(g, Color.BLUE,p.getTriangulationEdges());
        }

        //paint mswr
        if(paintArr[4]&&p.getMswr()!=null){
            paintLineArray(g, Color.green,p.getMswr());
        }

        if(!edges.isEmpty()){
            Graphics2D g7 = (Graphics2D) g;
            g7.setColor(Color.cyan);
            g7.draw(edges.get(0));

        }

        Graphics2D g8 = (Graphics2D) g;
        g8.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g8.setColor(Color.BLACK);
        g8.drawString("Route Dist: "+ p.getDistRoute(p.getMswr()),10,20);
        g8.drawString("Delay: "+p.getDelay(),10,30);

    }

    public void paintLineArray(Graphics g, Color c, List<Line2D.Double> arr){
        for (Line2D.Double e : arr) {
            Graphics2D g1 = (Graphics2D) g;
            g1.setColor(c);
            g1.draw(e);
        }
    }

    public void paintDCELPoly(Graphics g, Color c, DCELStructure halfEdges){
        HalfEdge start = halfEdges.getStart();
        Graphics2D g1 = (Graphics2D) g;
        g1.setColor(c);
        g1.draw(start.getLine());

        HalfEdge edge = start.getNext();
        Line2D.Double a;
        while(!halfEdges.getStart().equal(edge)){
            g1 = (Graphics2D) g;
            g1.setColor(c);
            a = edge.getLine();
            g1.draw(a);
            edge = edge.getNext();
        }
    }

    private void addEdge(Point2D.Double start, Point2D.Double end) {
        //  Add the Edge to the List so it can be repainted
        p.addEdge(start, end);
        repaint();
    }

    public Polygon getPolygon(){
        return this.p;
    }

    public void setPolygon(Polygon p){
        this.p=p;
    }

    public void clear() {
        this.p = new Polygon();


        ml.clear();
        repaint();
    }

    /**
     * For rectilinear Polygons only!
     * This function fixes the edges and vertex array for the
     * rectilinear mode.
     * It is nearly impossible for the user to close the polygon
     * manually.
     * Fix the last vertex & the last two lines in the arraylist.
     */
    public void fixEdges(){
        List<Line2D.Double> edges = p.getEdgeList();
        List<Point2D.Double> vertex = p.getVertexList();
        int lastIndex = edges.size()-1;

        // Go to the second last edge
        Line2D.Double last_line = p.getEdgeList().get(lastIndex-1);

        /*
        * Fix last vertex and then the length of the second last edge
         */

        //second last edge is horizontal
        if(last_line.getX1()==last_line.getX2()){
            vertex.set(lastIndex,new Point2D.Double(last_line.getX1(),vertex.get(0).getY()));
            last_line.setLine(vertex.get(lastIndex-1),vertex.get(lastIndex));
            edges.set(lastIndex,last_line);
        //second last edge is vertical
        }else if(last_line.getY1()==last_line.getY2()){
            vertex.set(lastIndex,new Point2D.Double(vertex.get(0).getX(),last_line.getY1()));
            last_line.setLine(vertex.get(lastIndex-1),vertex.get(lastIndex));
            edges.set(lastIndex,last_line);
        }


        //Fix last edge
        edges.set(lastIndex, new Line2D.Double(vertex.get(vertex.size()-1),vertex.get(0)));

        p.setEdgeList(edges);
        p.setVertexList(vertex);
    }

    /**
     * Calculate and return the angles between two lines of the type
     * Line2D.Double. The value is shown in the radiant unit.
     *
     * @param  line1    the first line
     * @param  line2    the second line
     * @return      the angle between these two lines
     */
    public static double angleBetween2Lines(Line2D.Double line1, Line2D.Double line2)
    {
        double angle1 = Math.atan2(line1.getY1() - line1.getY2(),
                line1.getX1() - line1.getX2());
        double angle2 = Math.atan2(line2.getY1() - line2.getY2(),
                line2.getX1() - line2.getX2());
        return angle1-angle2;
    }

    class PopUpDemo extends JPopupMenu implements ActionListener {
        JMenuItem anItem;
        Point mouse;

        public PopUpDemo(Point mouse) {

            this.mouse = mouse;
            anItem = new JMenuItem("Delete");
            anItem.addActionListener(this);
            add(anItem);
            anItem = new JMenuItem("Change order number");
            anItem.addActionListener(this);
            add(anItem);
        }


        @Override
        public void actionPerformed(ActionEvent e) {
            Point2D tmp = (Point2D) mouse;
            if ("Delete".equals(e.getActionCommand())) {

                //ACTION FOR DELETE
            } else if ("Change order number".equals(e.getActionCommand())) {
                //ANOTHER ACTION
            }

        }
    }

    class MyMouseListener extends MouseInputAdapter {
        private Point2D.Double lastPoint = new Point2D.Double();
        private Point2D.Double currentPoint = new Point2D.Double();
        private Ellipse2D.Double a = null;  // circle to close the polygon manually
        private boolean flag = false;       //flag if the mouse drag line is shown
        List<Point2D.Double> vertex = p.vertexList;
        List<Line2D.Double> edges = p.edgeList;


        public void clear(){
            lastPoint = new Point2D.Double();
            currentPoint = new Point2D.Double();
            a = null;  // circle to close the polygon manually
            flag = false;       //flag if the mouse drag line is shown
            vertex = p.vertexList;
            edges = p.edgeList;
        }
        /**
         * Function if a mouse button is pressed.
         * For User building the polygon manually
         */
        public void mousePressed(MouseEvent e) {

            //left mouse button pressed
            if (e.getButton() == MouseEvent.BUTTON1) {

                //first left mouse push - push vertex
                if (vertex.isEmpty() && edges.isEmpty()) {
                    currentPoint = new Point2D.Double(e.getX(), e.getY());
                    p.addVertex(currentPoint);
                    lastPoint = currentPoint;

                    // circle to close the polygon manually
                    a = new Ellipse2D.Double(currentPoint.getX()-10, currentPoint.getY()-10, 20, 20);
                    //uncomment to show circle
                    c=a;

                    flag = true;
                } else if(!vertex.isEmpty()){  //for the next mouse clicks


                    //updating lastPoint & currentPoint
                    lastPoint = currentPoint;

                    /*
                     * For rectilinear mode
                     * First if statement:
                     * After the first edge is set (after two left mouse clicks). Recognizes from the last edge
                     * where to set current point
                     *
                     * Else statement:
                     * no edges. Notice from the angle where current point is set.
                     */
                    /*if(!edges.isEmpty()){
                        Line2D.Double last_line=edges.get(edges.size()-1);
                        if(last_line.getX1()==last_line.getX2()){
                            //line = new Line2D.Double(currentPoint, new Point2D.Double(e.getX(), last_line.getY2()));
                            currentPoint = new Point2D.Double(e.getX(), last_line.getY2());

                        }else if(last_line.getY1()==last_line.getY2()){
                            //line = new Line2D.Double(currentPoint, new Point2D.Double(last_line.getX2(), e.getY()));
                            currentPoint = new Point2D.Double(last_line.getX2(), e.getY());
                        }


                    }
                    else{
                        //currentPoint = new Point2D.Double(e.getX(), e.getY());
                        Point2D.Double tmp = new Point2D.Double(lastPoint.getX()+1,lastPoint.getY()-1);
                        Line2D.Double line1 = new Line2D.Double(lastPoint,tmp);

                        Line2D.Double line2 = new Line2D.Double(lastPoint,new Point2D.Double(e.getX(), e.getY()));

                        double angle = angleBetween2Lines(line1,line2);

                        System.out.println(angle);

                        // vertical line
                        if(angle<(Math.PI/2)||(angle>(Math.PI/2)&& angle>Math.PI && angle<(3*Math.PI/2))){
                            currentPoint = new Point2D.Double(lastPoint.getX(), e.getY());
                        }
                        // horizontal line
                        else if((angle>(Math.PI/2) && angle<Math.PI) ||
                                (angle>(3*Math.PI/2)&&angle<2*Math.PI)){
                            currentPoint = new Point2D.Double(e.getX(), lastPoint.getY());
                        }

                    }*/
                    //System.out.println(flag);

                    //nonrectilinear
                    currentPoint = new Point2D.Double(e.getX(), e.getY());

                    //current point is within the circle. The user gave all the vertices to built the polygon
                    if (a.contains(new Point2D.Double(currentPoint.getX(),currentPoint.getY()))) {
                        //System.out.println("if1 "+edges);
                        addEdge(lastPoint, vertex.get(0));
                        line = null;
                        c=null;
                        //================================================================================
                        //test
                        //================================================================================

                        //EssentialVertex a = new EssentialVertex(edges,vertex);
                        //a.getMin_max_array();

                        //================================================================================
                        fixEdges();
                        repaint();
                        flag = false;
                    }
                    // The building of the polygon is not finished yet
                    else{
                        //System.out.println("if2");
                        addEdge(lastPoint,currentPoint);
                        vertex.add(currentPoint);
                        line = null;
                        repaint();
                    }
                }
            }

            //If right button pressed
            else if (e.getButton() == MouseEvent.BUTTON3) {

                if (e.isPopupTrigger())
                    doPop(e);
            }
        }

        public void mouseMoved(MouseEvent e) {

            if(!edges.isEmpty() && flag ==true){
                Line2D.Double last_line=edges.get(edges.size()-1);
                if(last_line.getX1()==last_line.getX2()){
                    line = new Line2D.Double(currentPoint, new Point2D.Double(e.getX(), last_line.getY2()));
                    repaint();
                }else if(last_line.getY1()==last_line.getY2()){
                    line = new Line2D.Double(currentPoint, new Point2D.Double(last_line.getX2(), e.getY()));
                    repaint();
                }
            }
             if(!vertex.isEmpty() && flag ==true) {
                Point2D.Double tmp = new Point2D.Double(vertex.get(0).getX()+1,vertex.get(0).getY()-1);
                Line2D.Double line1 = new Line2D.Double(vertex.get(0),tmp);

                Line2D.Double line2 = new Line2D.Double(vertex.get(0),new Point2D.Double(e.getX(), e.getY()));

                //Uncomment for rectilinear mode
                /*double angle = angleBetween2Lines(line1,line2);

                // vertical line
                if(angle<(Math.PI/2)||(angle>(Math.PI/2)&& angle>Math.PI && angle<(3*Math.PI/2))){
                    line = new Line2D.Double(currentPoint, new Point2D.Double((int)currentPoint.getX(), e.getY()));
                }*/
                // horizontal line
                /*else if((angle>(Math.PI/2) && angle<Math.PI) ||
                        (angle>(3*Math.PI/2)&&angle<2*Math.PI)){
                    line = new Line2D.Double(currentPoint, new Point2D.Double(e.getX(), (int)currentPoint.getY()));
                }*/
                line = new Line2D.Double(currentPoint, new Point2D.Double(e.getX(), e.getY()));
                repaint();
            }

        }

        public void mouseDragged(MouseEvent e) {

        }

        public void mouseReleased(MouseEvent e) {

            if (e.getButton() == MouseEvent.BUTTON1) {


            }
            //If right button pressed
            else if (e.getButton() == MouseEvent.BUTTON3) {
                if (e.isPopupTrigger())
                    doPop(e);
            }
        }

        public void doPop(MouseEvent e) {
            PopUpDemo menu = new PopUpDemo(e.getPoint());
            menu.show(e.getComponent(), e.getX(), e.getY());
        }
    }
}

