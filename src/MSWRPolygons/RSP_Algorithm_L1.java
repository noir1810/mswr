package MSWRPolygons;

import DataStructures.DoubleResult;
import DataStructures.Polygon;
import DataStructures.LineContainer;
import DoubleConnectedEdge.DCELStructure;
import DoubleConnectedEdge.HalfEdge;
import DoubleConnectedEdge.Vertex;
import RayShooting.LineFunction;
import RayShooting.SimpleRayShooting;
import Triangulation.Triangulation;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.*;


public class RSP_Algorithm_L1 {

    Polygon p;
    DCELStructure triangulation;
    SimpleRayShooting shooting;

    class CutContainer{
        HalfEdge start;
        HalfEdge end;
        Line2D.Double l;

        public CutContainer(HalfEdge start, HalfEdge end, Line2D.Double l){
            this.start = start;
            this.end = end;
            this.l = l;
        }

        public Line2D.Double getL() {
            return l;
        }
    }


    public RSP_Algorithm_L1(Polygon p){
        this.p = p;
        p.initEdgeArray(p.getVertexList());
    }

    private void initTriangulation() {
        Triangulation tri = new Triangulation(this.p);
        tri.algorithm();
        this.p.setTriangulationEdges(tri.getDrawTriangle());
        this.triangulation = tri.gethEdge();
    }

    private void initShooting(){
        if(this.triangulation == null){
            System.err.println("Init Triangulation first");
        } else shooting = new SimpleRayShooting(this.triangulation.getStart());
    }

    /****
     * checks via the determinate if the Vertex connected by two HalfEdges is an Reflex Vertex.
     * @param start starting HalfEdge
     * @param end   ending HalfEdge
     * @return  bool Value
     */
    public boolean isReflexVertex(HalfEdge start, HalfEdge end){
        double det;

        det = Triangulation.getDeterminate(start.getOrigin().getPoint(), end.getOrigin().getPoint(),
                end.getNext().getOrigin().getPoint());

        if(det>0){
            return true;
        }else return false;
    }

    /*******
     * Determines all Cutting edges on a reflex vertex, with the help of a shooting ray algorithm.
     * @return List of CutContainer
     */
    public List<CutContainer> getCuts(){
        HalfEdge edge = this.triangulation.getStart();
        Vertex end = edge.getOrigin();
        HalfEdge prev;
        List<CutContainer> cutArray = new ArrayList<>();
        DoubleResult shootRes;
        Line2D.Double tmpLine;

        do{
            prev = edge.getPrevious();

            if(isReflexVertex(prev, edge)){
                shootRes = this.shooting.shoot(prev,null);
                cutArray.add(new CutContainer(prev, (HalfEdge) shootRes.getRes2(), (Line2D.Double) shootRes.getRes1()));
                shootRes = this.shooting.shoot(edge.getTwin(), null);
                tmpLine = new Line2D.Double(((Line2D.Double) shootRes.getRes1()).getP2(),
                         ((Line2D.Double) shootRes.getRes1()).getP1());
                cutArray.add(new CutContainer((HalfEdge) shootRes.getRes2(), edge.getTwin(),
                        tmpLine));
            }
            edge = edge.getNext();
        }while(!(edge.getOrigin().equal(end)));

        return cutArray;
    }

    /***
     * Helper Function for is right. Needed when container 2 has the same index an container1 end or start.
     * @param container
     * @param container2
     * @return
     */
    boolean checkhelper(CutContainer container, CutContainer container2){
        int start = container.start.getIndex();
        int end = container.end.getIndex();
        int index = container2.end.getIndex();

        double det;



        if(index == end){
            det = Triangulation.getDeterminate(container.getL().getP2(), container.getL().getP1(), container2.getL().getP1());
            if(det<0) return false;
                else return true;
        }else if(index ==start){
            det = Triangulation.getDeterminate(container.getL().getP1(), container.getL().getP2(), container2.getL().getP2());
            if(det<0) return true;
            else return false;
        }

        return false;
    }

    /****
     * With the help of the index we can determine if the end Point of container2 lies on the right chain of container1.
     * @param container
     * @param container2
     * @return
     */
    boolean is_right(CutContainer container, CutContainer container2){
        int start = container.start.getIndex();
        int end = container.end.getIndex();
        int index = container2.end.getIndex();
        int tmp;

        if(start<end){
            if(index>start && index <end){
                //is right
                return false;
            }else if(index == end||index ==start){
                return checkhelper(container, container2);
            } return true;
        }else{
            tmp=start;
            start = end;
            end=tmp;

            if(index>start && index <end){
                //right

                return true;
            }else if(index == end||index ==start)
                return checkhelper(container, container2);
            else return false;
        }
    }


    public boolean getDominance(CutContainer dominator, CutContainer dominated){
        if(dominator.getL().intersectsLine(dominated.getL()))
            return false;
        if(is_right(dominated, dominator)&& !is_right(dominator, dominated)){
            return true;
        }return false;
    }

    public CutContainer getOneEssential(List<CutContainer> cuts){
        CutContainer essential=null;
        CutContainer line, line2;

        int index=0;
        for (int i =0;i<cuts.size()-1;i++){
            line = cuts.get(i);
            line2 = cuts.get(i+1);

            if(getDominance(line,line2)==false){
                //line dominates line2
                essential = line2;
            }else if(getDominance(line2,line)==false){
                essential = line;
            }
            if(essential!=null){
                break;
            }
            index =i;
            if(!getDominance(line2,line)){
                //line dominates line2
                essential = line2;
            }
            if(essential!=null){
                break;
            }
            index=i+1;

        }

        for(int j=index;j<cuts.size();j++){
            line = cuts.get(j);
            if(getDominance(line,essential)==true){
                //line dominates line2
                essential = line;
            }
        }
        return essential;
    }



    public List<CutContainer> getEssentials(List<CutContainer> cuts){
        ArrayList<CutContainer> res = new ArrayList<>();
        CutContainer initEssential = getOneEssential(cuts);
        res.add(initEssential);


        //sort for starting points of cuts you encounter through counterclockwise traversal
        Comparator<CutContainer> lineComp = new Comparator<CutContainer>() {
            @Override
            public int compare(CutContainer o1, CutContainer o2) {

                if (o1.start.getIndex()==o2.start.getIndex()){
                    if(o1.start.getOrigin().getPoint().getX()
                            < o1.start.getNext().getOrigin().getPoint().getX()){
                        return Double.compare(o1.l.getX1(), o2.l.getX1());
                    }else return Double.compare(o2.l.getX1(), o1.l.getX1());



                }else
                    return Integer.compare(o1.start.getIndex(),o2.start.getIndex());
            }
        };
        Collections.sort(cuts, lineComp);

        //where to start from cuts array? - find index of init Essential
        int startIndex=0;
        for(CutContainer l: cuts){
            if(l.equals(initEssential)){
                break;
            }
            startIndex++;
        }

        //start from init essential
        CutContainer current = initEssential;
        CutContainer loopLine;
        int endIndex=startIndex;


        for(int i=startIndex+1;i!=endIndex;i++) {

            if(i>cuts.size()-1)
                i=0;
            loopLine = cuts.get(i);

            if (!getDominance(current,loopLine)) {
                /*get rid of same top bottom*/
                    //if(current.getL().getP1().equals(loopLine.)) {
                        res.add(loopLine);
                        current = loopLine;
                    //}
            }
            /*else if(loopLine.getLine().intersectsLine(current.getLine())){
                res.add(loopLine);
                current = loopLine;
            }*/
            if(i==cuts.size()-1)i=-1;
        }
        return res;
    }




    public void deleteLinesHelper(int start, int end, List<LineHelper> list){
        Line2D.Double line, line2;
        line = list.get(end).line;

        int i=start;
        do {
            list.get(i).bool=true;
            line2 = list.get(i).line;
            i--;
        }while(!LineContainer.lineEqual(line, line2));

    }

    public void deleteLines(int start, int end, List<LineHelper> list){
        Line2D.Double line, line2;
        if(start<end){
            deleteLinesHelper(list.size()-1, end, list);
            deleteLinesHelper(start, 0, list);
        }else{
            deleteLinesHelper(start, end, list);
        }
    }



    public boolean equalPoint(Point2D a, Point2D b){
        if(a.getX()==b.getX() && a.getY()==b.getY()){
            return true;
        }
        return false;
    }

    /***
     * Gets a boolean if line1 intersects in P1 or P2 in l2
     * @param l1
     * @param l2
     * @return
     */
    public Point2D getIntersection(Line2D l1, Line2D l2){

        Point2D intersect;
        //fixLine.line.contains(l.getLine().getP1());
        LineFunction a = new LineFunction(l1.getP1(), l1.getP2());
        LineFunction b = new LineFunction(l2.getP1(), l2.getP2());
        intersect = a.intersects(b);
        return intersect;
    }

    public List<Line2D.Double> getRoutePolygon(List<LineContainer> essentials, List<LineHelper> copy){
        //cutting Polygon
        LineHelper startLine, fixLine;
        Line2D.Double fixedLine;
        Point2D intersect;
        List<Line2D.Double> fixes = new ArrayList<>();

        for(LineContainer l : essentials){
            //fix endpoint or startpoint?
            startLine = copy.get(l.getStartIndex());
            //endLine = copy.get(l.getEndIndex());

            if(equalPoint(startLine.line.getP1(),l.getLine().getP1())
                    || equalPoint(startLine.line.getP2(),l.getLine().getP1())){
                //fix Endpoint
                fixLine = copy.get(l.getEndIndex());
                if(l.getEndIndex()==copy.size()-1){
                    deleteLines(l.getStartIndex(),0,copy);
                }else deleteLines(l.getStartIndex(),l.getEndIndex()+1,copy);

                intersect = getIntersection(l.getLine(),fixLine.line);
                fixedLine = new Line2D.Double(fixLine.line.getP1(), intersect);
                fixLine.setLine(fixedLine);
                //fixedLine = new Line2D.Double(fixLine.line.getP1(), intersect);
            }else {
                fixLine = copy.get(l.getStartIndex());
                if(l.getStartIndex()==0){
                    deleteLines(copy.size()-1,l.getEndIndex(),copy);
                }
                else if(l.getStartIndex()!=0)
                    deleteLines(l.getStartIndex()-1,l.getEndIndex(),copy);
                intersect = getIntersection(l.getLine(),fixLine.line);
                fixedLine = new Line2D.Double(intersect, fixLine.line.getP2());
                fixLine.setLine(fixedLine);
            }
            fixLine.line = fixedLine;
            //fixes.add(fixedLine);
        }
        return fixes;
    }


    public List<Line2D.Double> getRouteEssential(List<LineContainer> essentials){
        Line2D.Double ess1;
        Line2D.Double ess2;
        Point2D intersect;
        double det;
        List<Line2D.Double> res = new ArrayList<>();

        for(LineContainer container: essentials){
            res.add(new Line2D.Double(container.getLine().getX1(),container.getLine().getY1(),
                    container.getLine().getX2(), container.getLine().getY2()));
        }


        for(int i=0;i<essentials.size();i++){
            for(int j=i;j<essentials.size();j++) {
                ess1 = res.get(i);
                ess2 = res.get(j);
                if(j==i)continue;

                if (ess1.intersectsLine(ess2)) {
                    intersect = getIntersection(ess1, ess2);

                    det = Triangulation.getDeterminate(ess2.getP2(),ess2.getP1(),ess1.getP2());
                    //res.add(new Line2D.Double(intersect, ess1.getLine().getP2()));
                    if(det<0)
                        res.get(i).setLine(new Line2D.Double(intersect, ess1.getP2()));
                    else res.get(i).setLine(new Line2D.Double(ess1.getP1(), intersect));
                    //res.add(new Line2D.Double(ess2.getLine().getP1(), intersect ));
                    det = Triangulation.getDeterminate(ess1.getP2(),ess1.getP1(),ess2.getP2());
                    if(det<0)
                        res.get(j).setLine(new Line2D.Double( intersect,ess2.getP2()));
                    else res.get(j).setLine(new Line2D.Double(ess2.getP1(), intersect));
                    //i++;
                } else res.add(ess1);
            }
        }

        /*LineContainer ess0 = essentials.get(essentials.size()-2);
        ess1 = essentials.get(essentials.size()-1);
        ess2 = essentials.get(0);
        if(!ess0.getLine().intersectsLine(ess1.getLine())) {
            if (ess1.getLine().intersectsLine(ess2.getLine())) {
                intersect = getIntersection(ess1.getLine(), ess2.getLine());
                res.add(new Line2D.Double(intersect, ess1.getLine().getP2()));
                res.add(new Line2D.Double(ess2.getLine().getP1(), intersect));
               res.remove(0);
            } else res.add(ess1.getLine());
        }*/

        return res;
    }


    public List<Line2D.Double> getFinalRouteMSWR(List<LineContainer> essentials) {
        List<LineHelper> copy = new ArrayList<>();
        List<Line2D.Double> fixedLines;
        LineHelper helper;
        for (Line2D.Double l : p.getEdgeList()) {
            helper = new LineHelper(new Line2D.Double(l.getP2(),l.getP1()), false);
            copy.add(helper);
        }

        //get essentialRoute
        List<Line2D.Double> cuttedEssential = getRouteEssential(essentials);

        //cut polygon
        fixedLines = getRoutePolygon(essentials, copy);


        //start from init essential
        /*int startIndex = essentials.get(0).getStartIndex();
        int endIndex = essentials.get(0).getEndIndex();

        LineHelper loopline = copy.get(startIndex);
        Line2D.Double line;

        List<Line2D.Double> res = new ArrayList<>();
        if(cuttedEssential.get(0).getP2().equals(fixedLines.get(0).getP2()))
            //is the first fixed Line the first Line we travel?
            res.add(fixedLines.remove(0));
        res.add(cuttedEssential.remove(0));

        line = fixedLines.get(0);
        if (!loopline.bool) {
            res.add(loopline.line);
        }else if(line.getP1().equals(loopline.line.getP1())||line.getP2().equals(loopline.line.getP2())){
            res.add(fixedLines.remove(0));
        }

        if(startIndex+1 == copy.size()) startIndex = -1;
        for (int i = startIndex+1; i != endIndex; i++) {
            loopline = copy.get(i);

            if(!fixedLines.isEmpty())line = fixedLines.get(0);
            if (!loopline.bool) {
                res.add(loopline.line);
            }else if(!fixedLines.isEmpty()&&line.getP1().equals(loopline.line.getP1())||line.getP2().equals(loopline.line.getP2())){
                res.add(fixedLines.remove(0));
            }

            if (!cuttedEssential.isEmpty()) {
                line = cuttedEssential.get(0);
                if (line.intersectsLine(loopline.line)) {
                    res.add(cuttedEssential.remove(0));
                }
            }
            if (i+1 == copy.size()) i = -1;
        }*/
        List<Line2D.Double> res = new ArrayList<>();

        for(LineHelper help: copy){
            if(!help.bool) res.add(help.line);
        }
        if (!cuttedEssential.isEmpty()){
            while (!cuttedEssential.isEmpty())
                res.add(cuttedEssential.remove(0));
        }
        if (!fixedLines.isEmpty()){
            while (!fixedLines.isEmpty())
                res.add(fixedLines.remove(0));
        }
        return res;
    }

    public List<Line2D.Double> algorithmMswr(){
        List<Line2D.Double> res = new ArrayList<>();
        initTriangulation();
        initShooting();

        List<CutContainer> cutArr = getCuts();
        List<Line2D.Double> cutDraw = new ArrayList<>();
        for(CutContainer c : cutArr){
            cutDraw.add(c.l);
        }
        this.p.setCuttingList(cutDraw);

        CutContainer a = getOneEssential(cutArr);
        List<Line2D.Double> cDraw = new ArrayList<>();
        cDraw.add(a.l);

        List<CutContainer> essArr = getEssentials(cutArr);
        List<LineContainer> lArr = new ArrayList<>();
        cutDraw = new ArrayList<>();
        for(CutContainer c : essArr){
            cutDraw.add(c.l);
            lArr.add(new LineContainer(c.l, c.start.getIndex(), c.end.getIndex()));
        }
        this.p.setEssentialList(cutDraw);
        List<Line2D.Double> route = getFinalRouteMSWR(lArr);
        this.p.setMswr(route);

        return res;
    }

    public void printArr(ArrayList<Line2D.Double> a){
        for(Line2D.Double l:a){
            System.out.println(l.getP1()+" "+l.getP2());
        }
    }
}