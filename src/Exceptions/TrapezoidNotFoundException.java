package Exceptions;

public class TrapezoidNotFoundException extends Exception{
    public TrapezoidNotFoundException(String message){
        super(message);
    }
}
