package Exceptions;

public class NeighbourIntegrityException extends Exception{
    public NeighbourIntegrityException(String message){
        super(message);
    }

}
