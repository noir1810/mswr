package Exceptions;

public class EndPointAlreadyInsertedException extends Exception {

    public EndPointAlreadyInsertedException(String message){
        super(message);
    }

}
