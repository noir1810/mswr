package Exceptions;

public class SSNodeFalseClassException extends Exception{

    public SSNodeFalseClassException(String message){
        super(message);
    }
}
