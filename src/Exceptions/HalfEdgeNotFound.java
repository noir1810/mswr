package Exceptions;

public class HalfEdgeNotFound extends Exception{
    public HalfEdgeNotFound(String message){
        super(message);
    }
}
