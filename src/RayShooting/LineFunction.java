package RayShooting;

import java.awt.geom.Point2D;

public class LineFunction {
    double slope;
    double yCut;

    public LineFunction(Point2D a, Point2D b){
        this.slope = calcSlope(a, b);
        this.yCut = calcYCut(a, this.slope);

    }

    private double calcSlope(Point2D a, Point2D b){
        return (b.getY()-a.getY())/(b.getX()-a.getX());
    }

    private double calcYCut(Point2D a, double slope){
        return a.getY()-(slope*a.getX());
    }

    protected double getY(double x){
        return slope*x+yCut;
    }

    protected double getX(double y){
        if(slope==0){
            System.err.println("Cannot calculate x - slope is zero");
            return 0;
        }else return (y-this.yCut)/this.slope;
    }

    public Point2D intersects(LineFunction lineFunction){
        double cutdiff = this.yCut-lineFunction.yCut;
        double slopediff = lineFunction.slope-this.slope;
        double x;

        if(slopediff==0){
            System.err.println("Slopediff is zero");
            return null;
        }else x = cutdiff/slopediff;

        return new Point2D.Double(Math.round(x), Math.round(this.getY(x)));
    }

}
