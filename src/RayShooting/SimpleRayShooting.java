package RayShooting;

import DataStructures.DoubleResult;
import DoubleConnectedEdge.HalfEdge;
import DoubleConnectedEdge.Vertex;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

public class SimpleRayShooting {
    HalfEdge triangulatedStart;
    double maxX = 0;
    double minX = Double.POSITIVE_INFINITY;

    public SimpleRayShooting(HalfEdge triangulatedStart){
        this.triangulatedStart = triangulatedStart;
        init();
    }

    //determine maxX & minX
    void init(){
        HalfEdge edge = this.triangulatedStart;
        Vertex end = edge.getOrigin();
        this. maxX = edge.getOrigin().getPoint().getX();
        this. minX = edge.getOrigin().getPoint().getX();
        do{
            //hedgeList.add(new HalfEdge(edge));
            if(this.maxX < edge.getOrigin().getPoint().getX())
                this.maxX = edge.getOrigin().getPoint().getX();
            if(this.minX > edge.getOrigin().getPoint().getX())
                this.minX = edge.getOrigin().getPoint().getX();
            edge = edge.getNext();
        }while(!(edge.getOrigin().equal(end)));
    }


    /****
     * Extends a line to minX and maxX
     * @param start LinePoint a
     * @param end   LinePoint b
     * @return  the extended Line
     */
    private Line2D getExtendedLine(LineFunction lfunc, Vertex start, Vertex end){
        Line2D res = new Line2D.Double();
        Point2D newP = new Point2D.Double(this.minX, lfunc.getY(this.minX));
        Point2D newQ = new Point2D.Double(this.maxX, lfunc.getY(this.maxX));

        return new Line2D.Double(newP,newQ);
    }

    private boolean point_in_X_RangeOfLine(Line2D line, Point2D p){
        double minX, maxX;
        if(line.getX1()<line.getX2()){
            minX = line.getX1();
            maxX = line.getX2();
        }else{
            maxX = line.getX1();
            minX = line.getX2();
        }

        if(p.getX()>minX && p.getX()<maxX) return true;
            else return false;
    }

    /***
     * checks if on the current triangle is an edge intersecting with a line
     * @param lineFunction  LineFunction of line
     * @param line
     * @param triangleEdge  first Edge of the triangle, already has a intersect point
     * @return  intersecting Point, null if no intersection
     */
    private DoubleResult handleTriangle(LineFunction lineFunction, Line2D line, HalfEdge triangleEdge){

        DoubleResult res = new DoubleResult();
        triangleEdge = triangleEdge.getNext();
        Line2D triLine = new Line2D.Double(triangleEdge.getOrigin().getPoint(),
                triangleEdge.getNext().getOrigin().getPoint());
        Point2D intersect = lineFunction.intersects(new LineFunction(triangleEdge.getOrigin().getPoint(),
                                triangleEdge.getNext().getOrigin().getPoint()));
        if(!point_in_X_RangeOfLine(triLine, intersect)){
            intersect = null;
        }else if(!triLine.getP1().equals(intersect) && !triLine.getP2().equals(intersect)){
            res.setRes1(triangleEdge);
            res.setRes2(intersect);
            return res;
        }

        triangleEdge = triangleEdge.getNext();
        triLine = new Line2D.Double(triangleEdge.getOrigin().getPoint(),
                triangleEdge.getNext().getOrigin().getPoint());

        intersect = lineFunction.intersects(new LineFunction(triangleEdge.getOrigin().getPoint(),
                    triangleEdge.getNext().getOrigin().getPoint()));
        if(!point_in_X_RangeOfLine(triLine, intersect)){
            return null;
        }else if(!triLine.getP1().equals(intersect)&&!triLine.getP2().equals(intersect)){
            res.setRes1(triangleEdge);
            res.setRes2(intersect);
            return res;
        } else return null;
    }

    /****
     * Performs the ray shooting. Make shure to give the half edge which is already in the right direction.
     * @param edge HalfEdge already in the right direction
     * @return  the shooting line with the intersected HalfEdge
     */
    public DoubleResult shoot(HalfEdge edge, Line2D beeLine){
        LineFunction lfunc = new LineFunction(edge.getOrigin().getPoint(), edge.getNext().getOrigin().getPoint());

        Line2D line;
        if(beeLine==null)
            line = getExtendedLine(lfunc, edge.getOrigin(), edge.getNext().getOrigin());
        else line = beeLine;

        Point2D startP = edge.getNext().getOrigin().getPoint();
        HalfEdge currentTriangle;
        if(edge.getFace()==null) currentTriangle = edge.getTwin();
            else currentTriangle = edge;
        Point2D intersectP=null;

        DoubleResult res = handleTriangle(lfunc, line, currentTriangle);
        if(res!=null) intersectP = (Point2D.Double) res.getRes2();


        while(currentTriangle.getFace()!=null){
            res = handleTriangle(lfunc, line, currentTriangle);
            if(res!=null){
                intersectP = (Point2D.Double) res.getRes2();
                currentTriangle = ((HalfEdge) res.getRes1()).getTwin();
            }else if(currentTriangle.getNext().getNext().getNext().getOrigin().getPoint().equals(startP)
                    ||currentTriangle.getOrigin().getPoint().equals(startP))
                currentTriangle = currentTriangle.getNext().getNext().getTwin();
            else currentTriangle = currentTriangle.getNext().getTwin();
        }
        return new DoubleResult(new Line2D.Double(startP, intersectP), currentTriangle);
    }
}
