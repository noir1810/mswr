package DoubleConnectedEdge;

import java.awt.geom.Line2D;

public class HalfEdge{
    HalfEdge next;
    HalfEdge previous;
    HalfEdge twin;
    Face face;
    Vertex origin;
    int index;
    boolean essential;

    public HalfEdge(){
    }

    public HalfEdge(HalfEdge e){
        this.next = e.next;
        this.previous = e.getPrevious();
        this.next = e.next;
        this.twin = e.twin;
        this.face = e.face;
        this.origin = e.origin;

    }

    public boolean equal(HalfEdge hEdge){
        if(this.face == hEdge.face
                && this.twin==hEdge.twin && this.origin == hEdge.origin)
            return true;
        return false;
    }

    public Vertex originTwin(){
        return twin.origin;
    }

    public Line2D.Double getLine(){
        Vertex twin = originTwin();
        return new Line2D.Double(origin.x,origin.y,twin.x,twin.y);
    }

    public void setNext(HalfEdge next) {
        this.next = next;
    }

    public void setPrevious(HalfEdge previous) {
        this.previous = previous;
    }

    public void setTwin(HalfEdge twin) {
        this.twin = twin;
    }

    public void setFace(Face face) {
        this.face = face;
    }

    public void setOrigin(Vertex origin) {
        this.origin = origin;
    }

    public HalfEdge getTwin() {
        return twin;
    }

    public HalfEdge getNext() {
        return next;
    }

    public Face getFace() {
        return face;
    }

    public Vertex getOrigin() {
        return origin;
    }

    public HalfEdge getPrevious() {
        return previous;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setEssential(boolean essential) {
        this.essential = essential;
    }

    public boolean getEssential() {
        return this.essential;
    }

}
