package DoubleConnectedEdge;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import DataStructures.DoubleResult;
import DataStructures.Polygon;
import Exceptions.HalfEdgeNotFound;
import Triangulation.Triangulation;

public class DCELStructure {
    Polygon poly;
    HalfEdge start;
    List<Face> faceList = new ArrayList<>();

    public DCELStructure(Polygon poly){
        this.poly = poly;
        initStructure();
    }

    public HalfEdge getStart() {
        return start;
    }

    public void initStructure(){
        List<Point2D.Double> pVertexList = poly.getVertexList();
        List<Vertex> vertexList = new ArrayList<>();

        for(Point2D.Double p : pVertexList){
            vertexList.add(new Vertex(p));
        }

        List<HalfEdge> h1List = new ArrayList<>();
        Vertex v1;
        Vertex v2;
        HalfEdge h1;
        HalfEdge h2;

        //create half Edge Pairs
        for(int i=0;i<vertexList.size();i++){
            v1 = vertexList.get(i);
            if(i==vertexList.size()-1)
                v2 = vertexList.get(0);
            else v2 = vertexList.get(i+1);

            h1 = new HalfEdge();
            h2 = new HalfEdge();

            h1.setIndex(i);
            h2.setIndex(i);

            h1.setOrigin(v2);
            v2.sethEdge(h1);
            h1.setTwin(h2);
            h1List.add(h1);

            h2.setOrigin(v1);
            v1.sethEdge(h2);
            h2.setTwin(h1);
        }

        HalfEdge h0;
        HalfEdge twin;
        Face face = new Face(h1List.get(0));
        faceList.add(face);
        this.start = h1List.get(0);

        for(int i=0;i<h1List.size();i++){

            h1 = h1List.get(i);
            if(i==h1List.size()-1) h2 = h1List.get(0);
                else h2 = h1List.get(i+1);
            if(i==0) h0 = h1List.get(h1List.size()-1);
                else h0 = h1List.get(i-1);

            h1.setNext(h0);
            h1.setPrevious(h2);
            h1.setFace(face);

            twin = h1.getTwin();
            twin.setPrevious(h0.twin);
            twin.setFace(null);
            twin.setNext(h2.twin);
        }
       if(this.start.face!=null) this.start = this.start.twin;
        System.out.println();
    }

    public DoubleResult search_in_DCELS(Line2D line, HalfEdge start) throws HalfEdgeNotFound{
        HalfEdge edge = start;
        Vertex end = edge.getOrigin();
        Point2D a = line.getP1();
        Point2D b = line.getP2();
        DoubleResult res = new DoubleResult();

        do{
            if(edge.getOrigin().getPoint().equals(a) || edge.getOrigin().getPoint().equals(b)){
                if(res.getRes1()==null){
                    res.setRes1(edge);
                }else{
                    res.setRes2(edge);
                    return res;
                }
            }
            edge = edge.getNext();
        }while(!(edge.getOrigin().equal(end)));

        if(res.getRes1()==null || res.getRes2()==null) throw new HalfEdgeNotFound("Line "+line.getP1()+" "+line.getP2()+" not found");
        return null;
    }



    public boolean addHalfEdge(Line2D line){
        //search
        HalfEdge faceStart;
        DoubleResult<HalfEdge> doubleEdge = null;
        for(Face f:this.getFaceList()){
            faceStart = f.gethEdge();
            try {
                doubleEdge = search_in_DCELS(line, faceStart);
            }catch (HalfEdgeNotFound e){

            }
        }
        if(doubleEdge==null) return false;
        if(doubleEdge.getRes1()==null || doubleEdge.getRes2()==null)
            try {
                throw new HalfEdgeNotFound("Line "+line.getP1()+" "+line.getP2()+" not found");
            } catch (HalfEdgeNotFound e) {
                System.err.println(e);
                return false;
            }

        HalfEdge h1 = doubleEdge.getRes1();
        HalfEdge h2 = doubleEdge.getRes2();

        /*double det = Triangulation.getDeterminate(h1.getOrigin().getPoint(),h1.getTwin().getOrigin().getPoint(),h2.getOrigin().getPoint());
        if(det<0) {
            return false;
        }*/


        if(!(h1!=null && h2!=null)){
            System.out.println( "Erooror Half Edge");
            return false;
        }

        HalfEdge add = new HalfEdge();
        HalfEdge addTwin = new HalfEdge();

        faceList.remove(h1.face); //deleting original face

        add.setOrigin(h1.getOrigin());

        add.setPrevious(h1.getPrevious());
        h1.getPrevious().setNext(add);

        add.setNext(h2);
        add.setTwin(addTwin);
        addTwin.setTwin(add);

        addTwin.setOrigin(h2.getOrigin());

        addTwin.setPrevious(h2.getPrevious());
        h2.getPrevious().setNext(addTwin);

        addTwin.setNext(h1);
        h1.setPrevious(addTwin);
        h2.setPrevious(add);

        Face aFace = new Face(add);
        Face bFace = new Face(addTwin);
        faceList.add(aFace);
        faceList.add(bFace);

        add.setFace(aFace);
        addTwin.setFace(bFace);


        Vertex end = add.getOrigin();
        do{
            add.setFace(aFace);
            add = add.getNext();
        }while(!(add.getOrigin().equal(end)));

        end = addTwin.getOrigin();
        do{
            addTwin.setFace(bFace);
            addTwin = addTwin.getNext();
        }while(!(addTwin.getOrigin().equal(end)));
        return true;
    }

    public List<Face> getFaceList() {
        return faceList;
    }
}
