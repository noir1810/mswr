package DoubleConnectedEdge;

public class Face {
    HalfEdge hEdge;

    public Face(HalfEdge hEdge){
        this.hEdge = hEdge;
    }

    public void sethEdge(HalfEdge hEdge) {
        this.hEdge = hEdge;
    }

    public HalfEdge gethEdge() {
        return hEdge;
    }
}
