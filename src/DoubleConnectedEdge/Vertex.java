package DoubleConnectedEdge;

import java.awt.geom.Point2D;
import java.util.Comparator;

public class Vertex{
    double x;
    double y;
    HalfEdge hEdge;

    public Vertex(Point2D p, HalfEdge hEdge){
        x = p.getX();
        y = p.getY();
        this.hEdge = hEdge;
    }

    public Vertex(Point2D p){
        x = p.getX();
        y = p.getY();
    }

    public Point2D getPoint(){
        return new Point2D.Double(x,y);
    }

    public void sethEdge(HalfEdge hEdge) {
        this.hEdge = hEdge;
    }

    public HalfEdge gethEdge() {
        return hEdge;
    }

    public boolean equal(Vertex v){
        if(this.x==v.x && this.y==v.y)
            return true;
        else return false;
    }
}
